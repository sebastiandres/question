#! /usr/bin/env python

################################################################################
################################################################################
###          Question : Shallow Water Equation Prototype Tester              ###
################################################################################
################################################################################

################################################################################
################################################################################
from core import ModifyUsingCustomWaterLevel
from core import ConvertFromCOMCOTFormat
from core import ConvertFromSBFFormat
from core import TestFromMeshSplitter
from core import TestFromMeshAdapter
from core import TestFromMeshCleaner
from core import TestFromBenchmark
from core import ArgumentHandler
from core import TestFromSIPAT
from core import TestFromOkada
from core import Default
from core import Help

import argparse
from scipy import version as scipy_version # Only needed check version
import sys
import os

def versiontuple(v):
    return tuple(map(int, (v.split("."))))

################################################################################
################################################################################
def main(options):
    """
    Main delegates the process to the libraries
    Runs the provided options and selects the type of test to be executed.
    **Input**: options dictionary created with the argparse library.
    **Output**: Depends on input, several outputs are possible.
    """
    # If no option was selected, help user and exit
    no_option_selected = all(options[k] is None for k in options)
    if no_option_selected:
        Help.print_help_message()
        sys.exit(-1)

    # If particular help was selected, help user and exit
    if options["help"] is not None:
        Help.print_specific_help_message(options)
        sys.exit(-1)

    # Run conditionally on the selected argument
    if (options["benchmark"] is not None):
        return TestFromBenchmark.generate(options)
    if (options["adapt"] is not None):
        return TestFromMeshAdapter.generate(options)
    if (options["clean"] is not None):
        return TestFromMeshCleaner.generate(options)
    if options["from_comcot"] is not None:
        return ConvertFromCOMCOTFormat.generate(options)
    if options["split"] is not None:
        return TestFromMeshSplitter.generate(options)
    if options["from_sbf"] is not None:
        return ConvertFromSBFFormat.generate(options)
    if options["okada_surface"] is not None:
        return TestFromOkada.generate(options)
    if options["sipat"] is not None:
        return TestFromSIPAT.generate(options)

    # QAS OPTIONS
    if (options["flat"] is not None):
        return ModifyUsingCustomWaterLevel.generate(options)
    if (options["bell"] is not None):
        return ModifyUsingCustomWaterLevel.generate(options)
    if (options["wave"] is not None):
        return ModifyUsingCustomWaterLevel.generate(options)
    if (options["surface_from_file"] is not None):
        return ModifyUsingCustomWaterLevel.generate(options)

    # Clean exit
    return sys.exit(0)

################################################################################
# This is the main usage, to be used to process the file
################################################################################
if __name__=="__main__":
    if versiontuple(scipy_version.version)<versiontuple("0.14"):
        print("Error: question requires scipy >=0.14. You have "+scipy_version.version)
        sys.exit(-1)
    BC_choices = Default.ImplementedBoundaryConditions()

    # Create the parser and options
    parser = argparse.ArgumentParser(prog="question.py",
                                     description="QUESTION: Quick User Engine of Systemic Test Instances and Operational Nexus",
                                     formatter_class=argparse.RawTextHelpFormatter,
                                     add_help=False
                                     )

    # Tunning the help
    parser.add_argument("-h","--help",
                        help=argparse.SUPPRESS,
                        type=str,
                        nargs="*"
                        )

    ################################################################################
    # Common arguments
    ################################################################################
    parser.add_argument("--to", metavar="folder",
                        help=argparse.SUPPRESS,
                        type=str)

    parser.add_argument("--from", metavar="folder",
                        help=argparse.SUPPRESS,
                        type=str)

    ########################N########################################################
    # DATA GENERATION: Generate data using predefined benchmark models
    ################################################################################
    parser.add_argument("--benchmark", metavar="benchmark_number",
                        help=argparse.SUPPRESS,
                        type=int, default=None)

    ## Additional parameters for the grid ##
    parser.add_argument("--dL",
                        help=argparse.SUPPRESS,
                        type=float, default=None)

    parser.add_argument("--NElems",
                        help=argparse.SUPPRESS,
                        type=float, default=None)

    parser.add_argument("--triangles",
                        help=argparse.SUPPRESS,
                        type=str, choices=["equilateral", "rectangular"], default=None)

    # Optional: Can specify a surface level (constant, column, gaussian or given by okada)
    parser.add_argument("--BC",
                        metavar=("BC"),
                        help=argparse.SUPPRESS,
                        type=str, choices=BC_choices, default=None)

    parser.add_argument("--BC_list",
                        metavar=("L", "R", "B", "T"),
                        help=argparse.SUPPRESS,
                        type=str, nargs=4, default=None)

    ################################################################################
    # DATA GENERATION: Generate data using COMCOT file
    ################################################################################
    parser.add_argument("--from_comcot", metavar="comcot_folder",
                        help=argparse.SUPPRESS,
                        type=str, default=None)

    parser.add_argument("--to_sbf", metavar="folder",
                        help=argparse.SUPPRESS,
                        type=str)

    ################################################################################
    # DATA GENERATION: Generate data using Simplified file
    ################################################################################
    parser.add_argument("--from_sbf", metavar="sbf_folder",
                        help=argparse.SUPPRESS,
                        type=str, default=None)

    parser.add_argument("--split",
                        help=argparse.SUPPRESS,
                        action='store_true', default=None)

    parser.add_argument("--reduce", metavar="h0",
                        help=argparse.SUPPRESS,
                        type=float, default=None)

    parser.add_argument("--clean", metavar="dtol",
                        help=argparse.SUPPRESS,
                        type=float, default=None)

    ################################################################################
    # DATA GENERATION: Generate data using sipat file
    ################################################################################
    parser.add_argument("--sipat", metavar="info_escenario",
                        help=argparse.SUPPRESS,
                        type=str, default=None)

    parser.add_argument("--line",
                        metavar=("n"),
                        help=argparse.SUPPRESS,
                        type=int, nargs='*', default=None)

    parser.add_argument("--id",
                        metavar=("sid"),
                        help=argparse.SUPPRESS,
                        type=int, nargs='*', default=None)

    ################################################################################
    # DATA MODIFICATION: changes the initial water surface using analytical formulas
    ################################################################################
    parser.add_argument("--flat",
                        metavar=("W0", "U0", "V0"),
                        help=argparse.SUPPRESS,
                        type=float, nargs=3, default=None)
    parser.add_argument("--bell",
                        metavar=("X0", "Y0", "H", "R"),
                        help=argparse.SUPPRESS,
                        type=float, nargs=4, default=None)
    parser.add_argument("--wave",
                        metavar=("X0", "Y0", "H", "W", "th"),
                        help=argparse.SUPPRESS,
                        type=float, nargs=5, default=None)


    ################################################################################
    # DATA MODIFICATION: changes the initial water surface using okada surface
    ################################################################################
    parser.add_argument("--okada_surface", metavar="okada_file",
                        help=argparse.SUPPRESS,
                        type=str, default=None)

    parser.add_argument("--surface_from_file", metavar="surface_file",
                        help=argparse.SUPPRESS,
                        type=str, nargs=2, default=None)

    ################################################################################
    # DATA MODIFICATION: refines the mesh
    ################################################################################
    parser.add_argument("--adapt",
                        help=argparse.SUPPRESS,
                        type=float, nargs='+', default=None)

    parser.add_argument("--exclude_range",
                        metavar=("Min_x_or_Lon", "Max_x_or_Lon", "Min_y_or_Lat", "Max_y_or_Lat"),
                        help=argparse.SUPPRESS,
                        type=float, nargs=4, default=None)

    parser.add_argument("--remove_polygon",
                        metavar=("x1 y1 x2 y2 x3 y3 ... xn yn"),
                        help=argparse.SUPPRESS,
                        type=float, nargs="+", default=None)

    parser.add_argument("--select_range",
                        metavar=("Min_x_or_Lon", "Max_x_or_Lon", "Min_y_or_Lat", "Max_y_or_Lat"),
                        help=argparse.SUPPRESS,
                        type=float, nargs=4, default=None)

    ################################################################################
    # Save bathymetry at nodes for algorithms with continuous bathymetry requirement
    ################################################################################
    parser.add_argument("--Bnodes",
	                help=argparse.SUPPRESS,
			action='store_const', const=True, default=None)

    ################################################################################
    # Parse the arguments and action based on those
    ################################################################################
    options = vars(parser.parse_args())

    # Check actions are performed
    main(options)
