from scipy.interpolate import griddata
from scipy.sparse import coo_matrix
from scipy.spatial import Delaunay
import matplotlib.pyplot as plt
import numpy as np
import time
import pickle
import sys
import os

import SBFFormat as SBF
import BoundaryConditions
import LonLatHandler
import Default
import Common
import MeshUtilities

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# GENERATES ALL THE REQUIRED MESH PROPERTIES
################################################################################
def generate(from_folder, to_folder):
    """
    Refines the mesh by loading from from_folder,
    using parameters in adapt_dict
    and saving into to_folder.
    """
    # Load the data (original mesh or current mesh values)
    if SBF.has_valid_format(from_folder):
        sbf_dict = SBF.load(from_folder)
    else:
        print("Folder not in SBF format")
        return sys.exit(-1)

    tri = Delaunay(sbf_dict['Mesh'])
    print("\nSplitting triangles.")
    print("\tStarted with {0:,} Nodes.".format(sbf_dict['Mesh'].shape[0]))
    # Helpers
    stringify = np.vectorize(lambda a,b: str(a)+";"+str(b))
    integerify = lambda pair_str: np.array(pair_str.split(';'), dtype=int)
    # Get unique sides to split
    sides_1 = tri.simplices[:,[0,1]]
    sides_1.sort(axis=1)
    sides_1_str = sides_1.astype(str)
    sides_1_str = stringify(sides_1_str[:,0], sides_1_str[:,1])
    sides_2 = tri.simplices[:,[1,2]]
    sides_2.sort(axis=1)
    sides_2_str = sides_2.astype(str)
    sides_2_str = stringify(sides_2_str[:,0], sides_2_str[:,1])
    sides_3 = tri.simplices[:,[0,2]]
    sides_3.sort(axis=1)
    sides_3_str = sides_3.astype(str)
    sides_3_str = stringify(sides_3_str[:,0], sides_3_str[:,1])
    all_sides_str = np.concatenate([sides_1_str, sides_2_str, sides_3_str])
    unique_sides_str = np.unique(all_sides_str)
    unique_sides = np.array([integerify(pair) for pair in unique_sides_str])
    # Split sides
    new_x = tri.points[:,0][unique_sides].mean(axis=1)
    new_y = tri.points[:,1][unique_sides].mean(axis=1)
    new_points = np.array([new_x, new_y]).T
    # Fit new bathymetry (do before changing the mesh)
    print("Interpolating to new Mesh")
    new_B = MeshUtilities.interpolate2D(new_points, sbf_dict['Mesh'], sbf_dict['Bathymetry'])
    sbf_dict['Bathymetry'] = np.hstack([sbf_dict['Bathymetry'], new_B])
    # Fit new mesh
    print("\tAdded {0:,} Nodes.".format(new_x.shape[0]))
    new_Mesh = np.vstack([sbf_dict['Mesh'], new_points])
    sbf_dict['Mesh'] = new_Mesh
    print("\tNew mesh has {0:,} Nodes.\n".format(new_Mesh.shape[0]))
    # Save the mesh
    SBF.save(to_folder, sbf_dict)

    return
