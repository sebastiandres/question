import numpy as np
import os
from .. import Common
from .. import Default

def time_serie_values(j):
    """
    Reads the analytical solution for the spatial profile
    """
    Dir  = __file__.replace(".pyc",".txt").replace(".py",".txt")
    Vals = np.loadtxt(Dir,dtype = float)

    t = Vals[:,0] 
    eta = Vals[:,j+1]

    return t, eta

def spatial_profile_values(x, y, t):
    """
    Computes the analytical solution for the dam break
    """
    return np.nan

def get_raw_values(dL, grid, terminal_BCs):
    """
    This laboratory case attempts to assess the runup on a complex beach. The aim of this case is first, to test the dry and wet 
    condition on the island and second, to study the time series developed in different locations along the beach. 
    """
    #==========================================================================================================================================
    # References
    #==========================================================================================================================================
    references = ["Sinolakis C., Bernard E., Titov V., Kanoglu U.,'Validation and Verification of Tsunami Numerical Models'," +
                  "Pure applied geophys 165 (2008) 2197-2228"]

    #==========================================================================================================================================
    # Suggested parameter usage
    #==========================================================================================================================================
    test_default_BCs = ["soft", "soft", "soft", "soft"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params =  {"Tmax":200.0, "g":Default.g(), "manning":0.0}

    # Convenient wrapper for the sech
    sech = lambda x: 1./np.cosh(x)

    # Suggested Model Parameters
    X1   =  -1.75   # [m] 
    g    =  9.810   # [m]
    d    =  0.1353  # [m]
    H    =  0.01619 # [m]

    # Discretization parameters
    xmin = 3.8500 # [m]
    xmax = 5.4880 # [m]
    ymin = 0.0000 # [m]
    ymax = 3.4020 # [m]

    # Related variables
    gamma = np.sqrt(0.75 * H/d**3) # [1/m]

    #core_path = os.path.dirname(__file__)
    #dest_folder = options["dest_folder"]
    #print core_path

    #==========================================================================================================================================
    # Bathymetry: (B)
    #==========================================================================================================================================
    B = np.loadtxt(os.path.join(files_path,"Bathymetry.txt"))

    #==========================================================================================================================================
    # Water Level Surface: (W)
    #==========================================================================================================================================
    W = np.loadtxt(os.path.join(files_path,"WaterLevel.txt"))

    #==========================================================================================================================================
    # Flux Velocities: (U0,V0)
    #==========================================================================================================================================
    U0 = np.loadtxt(os.path.join(files_path,"DischargeX.txt"))
    V0 = np.array([0.0])

    #==========================================================================================================================================
    # Mesh
    #==========================================================================================================================================
    Mesh = np.array([B[:,0],B[:,1]])
  
    #==========================================================================================================================================
    # Required Output
    #==========================================================================================================================================
    vectorized_eval = np.vectorize(spatial_profile_values)
    output = {}
    # Movie Animation Step
    output["movie_config"] =  {"dt_save":2.5, "compute_floading_and_arrival_time":True}

    # Time Series Config
    N_TS = 201
    x_TS = [4.5210, 4.5210, 4.5210]
    y_TS = [1.1960, 1.6960, 2.1960]
    output["TS_config"] = [x_TS, y_TS]
    t_TS = np.linspace(0, exec_params["Tmax"], N_TS)

    # Time Series Known Values
    output["TS_values"] = []
    for j in range(len(x_TS)):
	t_prof, w_prof = time_serie_values(j)
        output["TS_values"].append(t_prof)
        output["TS_values"].append(w_prof)

    # Spatial Profile Config
    N_SP = 201 
    t_SP = [0.0, 20.00, 80.00, 140.00, 180.00]
    x_SP  = np.linspace(xmin, xmax, N_SP)
    y_SP  = 1.666 + np.zeros(N_SP)
    output["SP_config"] = [t_SP, x_SP, y_SP]

    # Spatial Profile Values
    SP_values = []
    for t in t_SP:
        SP_values.append(vectorized_eval(x_SP, y_SP, t)) 
    output["SP_values"] = [t_SP, SP_values]

    #==========================================================================================================================================
    # Return dic with raw test values
    #==========================================================================================================================================
    return {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "output":output, "BC_list":test_BC, "references":references, "params":exec_params}
