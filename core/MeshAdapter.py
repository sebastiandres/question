from scipy.interpolate import griddata
from scipy.sparse import coo_matrix
from scipy.spatial import Delaunay
import matplotlib.pyplot as plt
import numpy as np
import time
import pickle
import sys
import os

import SBFFormat as SBF
import BoundaryConditions
import LonLatHandler
import Default
import Common
import MeshUtilities

if Default.DEBUG_MODE:
    from IPython import embed

PRINT_COUNT = 1         # How often we print the mesh adaptor statistics
INITIAL_MAX_POINTS_BY_FRAME_SIDE = 1000 # Discretization chosen for the frame

"""
IMPORTANT:
bbox is given as in the Persson paper, [xmin, ymin, xmax, ymax].
select_range is in [xmin, ymin, xmax, ymax].
remove_polygon is given as a list of points [x, y].
"""

################################################################################
# GENERATES ALL THE REQUIRED MESH PROPERTIES
################################################################################
def generate(from_folder, to_folder, adapt_dict):
    """
    Refines the mesh by loading from from_folder,
    using parameters in adapt_dict
    and saving into to_folder.
    """
    # Get the abs paths
    pickle_filepath_from = os.path.join(from_folder, "mesh_adapt.pkl")
    # Load the data (original mesh or current mesh values)
    if SBF.has_valid_format(from_folder):
        sbf_dict = SBF.load(from_folder)
    else:
        print("Folder not in SBF format")
        return sys.exit(-1)

    # Load the pickle file: original mesh values and iteration values
    if os.path.exists(pickle_filepath_from):
        with open(pickle_filepath_from, "r") as pickle_reader:
            pickle_dict = pickle.load(pickle_reader)
    else:
        pickle_dict = {}

    # Adapt the mesh and save it
    adapted_sbf_dict, pickle_dict = adapt(sbf_dict, pickle_dict, adapt_dict)

    # Save the mesh
    SBF.save(to_folder, adapted_sbf_dict)

    # Save the pickle
    pickle_filepath_to = os.path.join(to_folder, "mesh_adapt.pkl")
    with open(pickle_filepath_to, "w") as pickle_writer:
        pickle.dump(pickle_dict, pickle_writer)

    return

################################################################################
# GENERATES ALL THE REQUIRED MESH PROPERTIES
################################################################################
def adapt(sbf_dict, pickle_dict, adapt_dict):
    """
    Adapts the mesh to make it run efficiently with ANSWER.
    """
    # Common parameters
    Niters = adapt_dict["Niters"]
    interactive = False #adapt_dict["interactive"]
    # Conditional loading of parameters
    if len(pickle_dict)>0:
        # Load the parameters used in previous iterations and keep iterating
        print("\nStarting on iteration {0} of iterative mesh refiner".format(pickle_dict['TotalIterations']))
        print("Will run {0} more iterations\n".format(Niters))
        # Values stored in the pickle_dict
        domain_points = pickle_dict["domain_points"]
        if "exclude_bbox_points" in pickle_dict:
            remove_polygon = pickle_dict["exclude_bbox_points"]
        if "remove_polygon" in pickle_dict:
            remove_polygon = pickle_dict["remove_polygon"]
        element_size_proxy = pickle_dict["element_size_proxy"]
        hmin = pickle_dict["hmin"]
        iterStart = pickle_dict['TotalIterations']
        # Values stored in the sbf_dict
        NodeCoords = sbf_dict["Mesh"]
    else:
        ##################################################
        # Fresh start: compute everything from scratch
        ##################################################
        # Parameters
        print("!!! Update g with default value !!!")
        g  = 9.81
        remove_polygon = adapt_dict["remove_polygon"]
        select_bbox = adapt_dict["select_bbox"]
        mesh = sbf_dict["Mesh"]
        if select_bbox: # Use provided select_bbox if it exists
            xmin, ymin, xmax, ymax = select_bbox
            domain_points = np.array([[xmin, ymin],
                                      [xmax, ymin],
                                      [xmax, ymax],
                                      [xmin, ymax]])
        else:
            # Get bounding box [xmin, ymin, xmax, ymax]
            xmin, ymin = mesh.min(axis=0)
            xmax, ymax = mesh.max(axis=0)
            select_bbox = [xmin, ymin, xmax, ymax]
            # Get the domain points, assuming a shape like a truncated pyramid
            x, y = mesh.T
            y_bottom = ymin
            x_bottom_min = x[y==y_bottom].min()
            x_bottom_max = x[y==y_bottom].max()
            y_top = ymax
            x_top_min = x[y==y_top].min()
            x_top_max = x[y==y_top].max()
            domain_points = np.array([[x_bottom_min, y_bottom],
                                      [x_bottom_max, y_bottom],
                                      [x_top_max, y_top],
                                      [x_top_min, y_top]])
        # Get the excluded points
        """
        if len(exclude_bbox)==4:
            xmin_skip, ymin_skip, xmax_skip, ymax_skip = exclude_bbox
            remove_polygon = np.array([[xmin_skip, ymin_skip],
                                            [xmax_skip, ymin_skip],
                                            [xmax_skip, ymax_skip],
                                            [xmin_skip, ymax_skip]])
        else:
            remove_polygon = None
        """
        # We need a dt and hmin relative to the LonLat
        dt = adapt_dict["dt_mks"]
        hmin = adapt_dict["hmin_mks"]
        # Get the minimum edge side
        print("\nComputing adapted mesh using the following parameters:")
        print("\tRunning Niters={0:d}".format(Niters))
        print("\tTarget: dt={0:.5E}\n".format(dt))
        B = sbf_dict["Bathymetry"].T
        # Get the element size array
        element_size_proxy, hmin, hmax = get_relative_element_size_from_bathymetry(mesh, B, dt, hmin, g)
        # Minimum number of nodes is given for the largest h
        NNodes_min = int((xmax-xmin)/hmax) * int((ymax-ymin)/hmax)
        # Maximum number of nodes is given for the smallest h
        NNodes_max = int((xmax-xmin)/hmin) * int((ymax-ymin)/hmin)
        # Validate before continue
        print("Using the following parameters for mesh generation:")
        print("\tdt: {0:.3E} seconds".format(dt))
        print("\thmin: {0:.1f}".format(hmin))
        print("\thmax: {0:.1f}".format(hmax))
        print("\thmax/hmin: {0:.1f} (aspect ratio)\n".format(hmax/hmin))
        # Make them always fit the algorithm, or run out of memory
        max_points_by_frame_side = INITIAL_MAX_POINTS_BY_FRAME_SIDE # Initial estimation, should be kept
        #if (hmax/hmin > 0.5*max_points_by_frame_side):
        #    print("\t\tWARNING: ASPECT RATIO IS TOO LARGE. MIGHT RUN OUT OF MEMORY.")
        #    max_points_by_frame_side = 2*int(np.ceil(0.01* hmax/hmin)*100)
        #    print("\t\tWILL TRY WITH {0:,} points by side in each frame.".format(max_points_by_frame_side))

        print("\n\tMinimum number of Nodes: {0:,}".format(NNodes_min))
        print("\tMaximum number of Nodes: {0:,}".format(NNodes_max))
        # Initial points
        iterStart = 0
        fd = lambda p, domain_points=domain_points, remove_polygon=remove_polygon: MeshUtilities.compute_distance(p, domain_points, remove_polygon)
        NodeCoords = get_initial_points(select_bbox, element_size_proxy, hmin, hmax, fd,
                                        max_points_by_frame_side)
        # Add the corners
        corners = MeshUtilities.get_fix_points(domain_points, remove_polygon)
        NodeCoords = np.vstack([NodeCoords, corners])
        print("\nInitial points for iterative algorithm selected.")
        print("\tKept {0:,} nodes.".format(NodeCoords.shape[0]))

    #Strang's refiner
    NodeCoords, ExecutedIterations = distmesh2d(domain_points, remove_polygon,
                                                element_size_proxy, hmin,
                                                NodeCoords, iterStart, Niters)
    #ExecutedIterations = 0  # DELETE ME

    # Pack the solution
    adapted_sbf_dict = {}
    adapted_sbf_dict['Mesh'] = NodeCoords
    adapted_sbf_dict['Bathymetry'] = MeshUtilities.interpolate2D(new_mesh=NodeCoords,
                                                      old_mesh=sbf_dict["Mesh"],
                                                      old_values=sbf_dict["Bathymetry"])

    adapted_sbf_dict['CoordinateSystem'] = sbf_dict['CoordinateSystem']
    adapted_sbf_dict['LinearizationPoint'] = sbf_dict['LinearizationPoint']
    if 'TimeSeriesBuoyCoordinates' in sbf_dict:
        adapted_sbf_dict['TimeSeriesBuoyCoordinates'] = sbf_dict['TimeSeriesBuoyCoordinates']
    if 'SpatialProfileBuoyCoordinates' in sbf_dict:
        adapted_sbf_dict['SpatialProfileBuoyCoordinates'] = sbf_dict['SpatialProfileBuoyCoordinates']

    if 'TotalIterations' in pickle_dict:
        pickle_dict["TotalIterations"] += ExecutedIterations
    else:
        pickle_dict["domain_points"] = domain_points
        pickle_dict["remove_polygon"] = remove_polygon
        pickle_dict["element_size_proxy"] = element_size_proxy
        pickle_dict["hmin"] = hmin
        pickle_dict["TotalIterations"] = ExecutedIterations

    # Print stats one last time
    print("\nRefined mesh:")
    print("\t* Executed {0} iterations for a total of {1} accumulated iterations".format(ExecutedIterations, pickle_dict["TotalIterations"]))
    # Return computed values
    return adapted_sbf_dict, pickle_dict

################################################################################
################################################################################
def _get_bbox_mask(x, y, bbox):
    """
    Boolean mask of all elements that are inside the provided bounding box
    """
    xmin_aux, ymin_aux, xmax_aux, ymax_aux = bbox
    ind_xmin = ( xmin_aux <= x )
    ind_xmax = ( x <= xmax_aux )
    ind_ymin = ( ymin_aux <= y )
    ind_ymax = ( y <= ymax_aux )
    ind_x = np.logical_and(ind_xmin, ind_xmax)
    ind_y = np.logical_and(ind_ymin, ind_ymax)
    # Boolean with the nodes that should be kept always
    mask = np.logical_and(ind_x, ind_y)
    return mask

################################################################################
################################################################################
def get_relative_element_size_from_bathymetry(Mesh, B, dt, hmin, g=Default.g()):
    """
    B has dimensions [3,Ncols], the first two rows being the mesh.
    ESP = Element Size Proxy it gives a estimation of the element size, given
    so that all obtain a similar courant for bathymetry (and larger elements for
    a given threshold).
    ESP must have the following characteristics:
        - It has dimensions [3,Ncols], the first two rows being the mesh.
        - It has been adimnensionalized, so the min element has value 1.
    """
    ############################################################################
    # GET A COPY OF THE BATHYMETRY TO TWEAK AND ALTER LOCALLY
    ############################################################################
    b = np.abs(B)
    ############################################################################
    # COMPUTING THE ELEMENT EDGE SIZE BASED ON COURANT OF THE CELL
    ############################################################################
    # The worst case for rectangular triangles is the hypothenuse
    esp = 4 * np.sqrt(2) * dt * np.sqrt(g * b)
    esp[esp<hmin] = hmin
    hmax = esp.max()
    ############################################################################
    # TWEAKING THE TOPOGRAPHY TO GET FEWER INLAND ELEMENTS
    ############################################################################
    # Internal parameters
    land_threshold = 1000. # meters
    land_ratio = 1.5  # Land elements will be this larger compared to wer elements
    # Compute where and min-max values
    mask = (B > land_threshold)
    if np.any(mask):
        hmin_mask = esp[mask].min()
        hmax_mask = esp[mask].max()
        # Increase linealy, if possible
        if hmax_mask - hmin > 0:
           h_slope = (hmax_mask*land_ratio-hmin)/(hmax_mask - hmin) # slope of linear case
        else:
           h_slope = float("inf")
        if h_slope < 1E4: # Aspect ratio would be too much
            esp[mask] = hmin + h_slope*(esp[mask] - hmin)
            print("Making land elements larger")

    ############################################################################
    # NORMALIZE TO ASPECT RATIO, TO BE USED AS PROBABILITY
    ############################################################################
    esp = esp/hmin
    ESP = np.zeros([3,Mesh.shape[0]])
    ESP[:2,:] = Mesh.T
    ESP[2,:] = esp
    return ESP, hmin, hmax

################################################################################
################################################################################
def get_initial_points(bbox,
                       element_size_proxy, hmin, hmax,
                       fd, max_points_by_frame_side):
    """
    A better algorithm to get the initial points, using divide and conquer.
    Problems that have been detected and solved:
    * Too much memory required: Using frames of (at most) max_points_by_frame_side x max_points_by_frame_side
    * Rounding to Nx_frames or Ny_frames must take ceil to have less than max_points_by_frame_side by side
    # Dont overlap the borders of the frame
    * Mask elements so the interpolation is done only in the minimum number of elements
    * Mask enough elements so the interpolation actually has points to work with.
    * Start with less elements if h.min() is less than hmin in the frame.
    """

    # Distance Function d(x) (does not depends on the frame)
    #fd = lambda p: _distance_function(p, bbox)
    # Params
    xmin, ymin, xmax, ymax = bbox
    # Compute how many frames we need in x and y.
    # We take ceil so the actual number of points is less than max_points_by_frame_side
    h_frame = max(hmax, hmin*max_points_by_frame_side)  # All frames have at most this h
    Nx_frames = int(np.ceil((xmax-xmin)/h_frame))
    Ny_frames = int(np.ceil((xmax-xmin)/h_frame))
    print("\nUsing {0} frames in x and {1} frames in y".format(Nx_frames, Ny_frames))
    # Compute the boundaries in each direction
    x_borders = np.linspace(xmin, xmax, Nx_frames + 1)
    y_borders = np.linspace(ymin, ymax, Ny_frames + 1)
    # For each split, compute what you need and throw away points
    # based on the probability.
    # We dont need to adimensionalize by r0.max() because fh i is already in range [1., +inf]
    # Hence, 1./fh**2 is always less than 1
    N_total_frames = Nx_frames*Ny_frames
    start_time = time.time()
    geps = .10*hmin
    # Get a first raw interpolation for the centroid of each frame
    Lx_frame = (xmax-xmin)/Nx_frames
    Ly_frame = (ymax-ymin)/Ny_frames
    interpolator = MeshUtilities.ReusableInterpolator(element_size_proxy)
    # Process for each frame
    for j in range(Ny_frames):
        for i in range(Nx_frames):
            n_current_frame = j*Nx_frames + i + 1 # Better counting notation
            # Compute the new auxiliar bounding box
            xmin_aux = x_borders[i]
            xmax_aux = x_borders[i+1]
            ymin_aux = y_borders[j]
            ymax_aux = y_borders[j+1]
            # Get a mask for the original mesh with points on it
            frame_bbox = [xmin_aux, ymin_aux, xmax_aux, ymax_aux]
            n = 0.5
            bbox_aux = [xmin_aux - n*Lx_frame, ymin_aux - n*Ly_frame,
                        xmax_aux + n*Lx_frame, ymax_aux + n*Ly_frame]
            esp_mask_aux = _get_bbox_mask(element_size_proxy[0,:], element_size_proxy[1,:], bbox_aux)
            while esp_mask_aux.sum()<=3: # We must have at leat 4 points to make a good interpolation
                n += 1
                bbox_aux = [xmin_aux - n*Lx_frame, ymin_aux - n*Ly_frame,
                            xmax_aux + n*Lx_frame, ymax_aux + n*Ly_frame]
                esp_mask_aux = _get_bbox_mask(element_size_proxy[0,:], element_size_proxy[1,:], bbox_aux)
            # Now we know we have points to work with
            esp_frame = element_size_proxy[:,esp_mask_aux]
            min_esp = esp_frame[2, :].min()
            hmin_frame = hmin * min_esp # This will be the hmin used in this frame
            ## Take the longer but safest path. Fix later.
            #hmin_frame = hmin
            #min_esp = element_size_proxy[2, :].min()
            ## End of quick fix
            # We can always use the frame, because we computed the memory required beforehand
            include_right = True if i==(Nx_frames-1) else False
            include_top = True if j==(Ny_frames-1) else False
            # frame_grid includes right boundary or top boundary dynamically.
            x_aux, y_aux = frame_grid(xmin_aux, xmax_aux, ymin_aux, ymax_aux,
                                      hmin_frame, hmin_frame,
                                      include_right, include_top)
            # Stack them
            FrameNodeCoords = np.vstack((x_aux.flat, y_aux.flat)).T                # List of node coordinates
            # Keep only d<0 points. Actually, d<geps so some points will move to boundary
            FrameNodeCoords  = FrameNodeCoords[fd(FrameNodeCoords) < geps]
            point_candidates_in_frame = FrameNodeCoords.shape[0]
            # Disregard some points based on the edge length function
            fh_FrameNodeCoords = length_function(FrameNodeCoords, 1.0/min_esp, interpolator.interpolate)
            # Probability to get point
            if fh_FrameNodeCoords.shape[0]>0:
                r0 = 1.0/fh_FrameNodeCoords**2 # Probability to keep point
            else:
                r0 = -np.ones(FrameNodeCoords.shape[0]) # Keep with probability 1 if no point was computed
            #NodeCoords  = NodeCoords[np.random.random(NodeCoords.shape[0])<r0/r0.max()] # Rejection method
            FrameNodeCoords  = FrameNodeCoords[ np.random.random(FrameNodeCoords.shape[0])<r0 ] # Rejection method
            points_kept_in_frame = FrameNodeCoords.shape[0]
            # Add points to FinalNodeCoords
            if i==0 and j==0:
                FinalNodeCoords = FrameNodeCoords.copy()
            else:
                FinalNodeCoords = np.vstack([FinalNodeCoords, FrameNodeCoords])
            # Mesh estimation gets updated in each frame. 4 corners will be added at last
            mesh_size_est = 4 + int(FinalNodeCoords.shape[0]*float(N_total_frames)/n_current_frame)
            current_time = time.time()
            remaining_time = (current_time - start_time)*(float(N_total_frames)/n_current_frame - 1.)
            print("Processing frame {0:,} of {1:,}:".format(n_current_frame, N_total_frames))
            print("\thmin for frame: {0:.1f}".format(hmin_frame))
            print("\tStarted with {0:,} node candidates".format(point_candidates_in_frame))
            print("\tKeeping only {0:,} nodes".format(points_kept_in_frame))
            print("\tExpecting a mesh of size {0:,} Nodes".format(mesh_size_est))
            print("\tRemaining time {0:.1f} minutes for initial point generation.".format(remaining_time/60.))
    return FinalNodeCoords

################################################################################
################################################################################
################################################################################
def frame_grid(xmin, xmax, ymin, ymax, dLx, dLy, include_right, include_top):
    # Frames are better in rectangular way. At least, things seem more stable!
    # Rectangular and regular. Range takes the first but not the last
    # Rounding
    nx = int( np.round((xmax-xmin)/dLx) )
    ny = int( np.round((ymax-ymin)/dLy) )
    dLx = (xmax-xmin)/nx
    dLy = (ymax-ymin)/ny
    # Scaling to triangles
    dLx = dLx # No scaling
    dLy = np.sqrt(3)/2 * dLy
    x1, y1 = frame_grid_rect(xmin, xmax, ymin, ymax, dLx, dLy, include_right, include_top)
    x2, y2 = frame_grid_rect(xmin+0.5*dLx, xmax-0.5*dLx,
                             ymin+0.5*dLy, ymax-0.5*dLy, dLx, dLy, True, True)
    return np.hstack([x1,x2]), np.hstack([y1,y2])

def frame_grid_rect(xmin, xmax, ymin, ymax, dLx, dLy, include_right, include_top):
    # Frames are better in rectangular way. At least, things seem more stable!
    # Rectangular and regular. Range takes the first but not the last
    nx = int( np.round((xmax-xmin)/dLx) )
    ny = int( np.round((ymax-ymin)/dLy) )
    # This doesn't always use the exact dLx, but equispace the points more nicely
    if include_right:
        x2 = np.linspace(xmin, xmax, nx+1)
    else:
        x2 = np.linspace(xmin, xmax, nx+1)[:-1]
    # This doesn't always use the exact dLx, but equispace the points more nicely
    if include_top:
        y2 = np.linspace(ymin, ymax, ny+1)
    else:
        y2 = np.linspace(ymin, ymax, ny+1)[:-1]
    X, Y = np.meshgrid(x2, y2, indexing='xy')
    x_mesh, y_mesh = X.flatten(), Y.flatten()
    return x_mesh, y_mesh

################################################################################
# LENGTH FUNCTION
################################################################################
def length_function(points, scale, interpolator):
    """
    Signed element side function.
    """
    # Get the interpolation values: Why is ESP not being used?
    interps = interpolator(points)
    return scale*interps

################################################################################
################################################################################
def _dense(I, J, S, shape=None):
    """
    Similar to MATLAB's SPARSE(I, J, S, ...), but instead returning a
    dense array.
    **Input**: (I,J) are coords and S are values.
    **Output**: returns the associated dense matrix.

    Usage
    -----
    >>> shape = (m, n)
    >>> A = dense(I, J, S, shape, dtype)
    """

    # Advanced usage: allow J and S to be scalars.
    if np.isscalar(J):
        x = J
        J = np.empty(I.shape, dtype=int)
        J.fill(x)
    if np.isscalar(S):
        x = S
        S = np.empty(I.shape)
        S.fill(x)

    # Turn these into 1-d arrays for processing.
    S = S.flat; I = I.flat; J = J.flat
    return coo_matrix((S, (I, J)), shape).toarray()


################################################################################
################################################################################
def _unique_rows(A):
    """
    Similar to MATLAB's unique(A, 'rows'), this returns B
    where B is the unique rows of A
    **Input**:
    **Output**:

    Returns I if return_index is True
    Returns J if return_inverse is True
    """
    A = np.require(A, requirements='C')
    assert A.ndim == 2, "array must be 2-dim'l"

    orig_dtype = A.dtype
    ncolumns = A.shape[1]
    dtype = np.dtype((np.character, orig_dtype.itemsize*ncolumns))
    B = np.unique(A.view(dtype))

    B = B.view(orig_dtype).reshape((-1, ncolumns), order='C')

    return B


################################################################################
# MESH REFINEMENT ALGORITHM
################################################################################
def distmesh2d(domain_points, remove_polygon,
               element_size_proxy, hmin,
               p, startIter, nIters):
    """
    Based on the 2004 paper by Persson and Stang:
        http://persson.berkeley.edu/distmesh/persson04mesh.pdf
    See also:
        http://persson.berkeley.edu/distmesh/
    **Input**:
    **Output**:
    """

    # Change to True if you want to display the mesh
    interactive_refine = False
    # Parameters
    np.random.seed([0])
    print("\nRefining process started...")
    dptol  = 0.010
    ttol   = 0.500
    Fscale = 1.400 # original value 1.2
    deltat = 0.020
    geps   = 0.001*hmin
    deps   = np.sqrt(np.finfo(np.double).eps)*hmin
    densityctrlfreq = 5001 #45
    interpolator = MeshUtilities.ReusableInterpolator(element_size_proxy)
    # In the strang notation
    # Distance Function d(x)
    #fd = lambda p: _distance_function(p, bbox)
    # Element size function, h(x,y)
    fd = lambda p, domain_points=domain_points, remove_polygon=remove_polygon: MeshUtilities.compute_distance(p, domain_points, remove_polygon)
    fh = lambda p: length_function(p, 1.0, interpolator.interpolate)
    N  = p.shape[0]                                  # Number of points N
    cIter = startIter
    nIters = startIter + nIters
    pold = float('inf')                              # For first iteration

    # Abort if not enough nodes
    if N < 100:
        print("\nERROR: Try a smaller value for hmin when refining.")
        print("\tCurrent value hmin=%.5f is not small enough.\n" %hmin)
        print("\tGives N=%d points.\n" %N)
        sys.exit(-1)

    start_time = time.time()

    while True:

        # 3. Retriangulation by the Delaunay algorithm
        dist = lambda p1, p2: np.sqrt(((p1-p2)**2).sum(1))
        if (dist(p, pold)/hmin).max() > ttol:          # Any large movement?
            print "Larger Movement"
            pold = p.copy()                          # Save current positions
            tri = Delaunay(p)
            t = tri.vertices.copy()                        # List of triangles
            #Neigh = tri.neighbors.copy()
            pmid = p[t].sum(1)/3.                     # Compute centroids
            t = t[fd(pmid) < -geps]                  # Keep interior triangles

            # 4. Describe each bar by a unique pair of nodes
            bars = np.vstack((t[:, [0,1]], t[:, [1,2]], t[:, [2,0]]))  # Interior bars duplicated
            bars.sort(axis=1)
            bars = _unique_rows(bars)                 # Bars as node pairs

        if (cIter % PRINT_COUNT == 0):
            current_time = time.time()
            if cIter==startIter:
                average_iter_time = (current_time - start_time)
            else:
                average_iter_time = (current_time - start_time)/(cIter-startIter)
            remaining_time = average_iter_time * float(nIters-cIter)
            print("Iteration : " + str(cIter))
            print("\tRegular Points: {0:,}".format(p.shape[0]))
            print("\tRegular Cells:  {0:,}".format(t.shape[0]))
            print("\tOBS: Interpolation is computed from bathymetry with {0:,} points (larger is slower)".format(element_size_proxy.shape[1]))
            if cIter!=startIter:
                print("\tRemaining iteration time: {0:.1f} minutes".format(remaining_time/60.))

        cIter += 1

        # 6. Move mesh points based on bar lengths L and forces F
        barvec = p[bars[:,0]] - p[bars[:,1]]         # List of bar vectors
        L = np.sqrt((barvec**2).sum(1))              # L = Bar lengths
        hbars = fh(p[bars].sum(1)/2)
        L0 = (hbars*Fscale*np.sqrt((L**2).sum()/(hbars**2).sum()))  # L0 = Desired lengths
        # Density control - remove points that are too close
        if (cIter % densityctrlfreq) == 0 and (L0 > 2*L).any():
            ixdel = bars[L0 > 2*L].reshape(-1)
            p = p[np.setdiff1d(np.arange(N), ixdel)]
            N = p.shape[0]; pold = float('inf')
            continue

        F = L0-L
        F[F<0] = 0
        Fvec = F[:,None]/L[:,None].dot([[1,1]])*barvec # Bar forces (x,y components)
        Ftot = _dense(bars[:,[0,0,1,1]], np.repeat([[0,1,0,1]], len(F), axis=0), np.hstack((Fvec, -Fvec)), shape=(N, 2))
        p += deltat*Ftot                             # Update node positions

       # 7. Bring outside points
        d = fd(p); # Compute distances
        dtol = 0.40*hmin
        ix1 = d>0 # d>0 is outside
        ix2 = np.logical_and(-dtol<d, d<0.) # Find points inside (d<0)that are close to boundary
        ix = np.logical_or(ix1, ix2)
        if ix.any():
            dgradx = (fd(p[ix]+[deps,0])-d[ix])/deps # Numerical
            dgrady = (fd(p[ix]+[0,deps])-d[ix])/deps # gradient
            dgrad2 = dgradx**2 + dgrady**2
            p[ix] -= (d[ix]*np.vstack((dgradx, dgrady))/dgrad2).T # Project

        # 8. Termination criterion: All interior nodes move less than dptol (scaled)
        # CHeck the termination criterium
        if (cIter >= nIters): # or (np.sqrt((deltat*Ftot[d<-geps*10]**2).sum(1))/hmin).max() < dptol: #3250
            break # Get out of the loop

    print("Refining process is done...")
    print("Total of {0} nodes and {1} cells before adding Ghost Cells".format(p.shape[0], t.shape[0]))
    """
    # 9 Force points to boundary
    d = fd(p); # Compute distances
    ix1 = d>0 # d>0 is outside
    dtol = 0.50*hmin
    ix2 = np.logical_and(-dtol<d, d<0.) # Find points inside (d<0)that are close to boundary
    ix = np.logical_and(ix1, ix2)
    while ix.any():
        dgradx = (fd(p[ix]+[deps,0])-d[ix])/deps # Numerical
        dgrady = (fd(p[ix]+[0,deps])-d[ix])/deps # gradient
        dgrad2 = dgradx**2 + dgrady**2
        p[ix] -= (d[ix]*np.vstack((dgradx, dgrady))/dgrad2).T # Project
        # Compute the points not in boundary
        ix1 = d>0 # d>0 is outside
        ix2 = np.logical_and(-dtol<d, d<0.) # Find points inside (d<0)that are close to boundary
        ix = np.logical_or(ix1, ix2)

    # 10 Remove points that ended up being too close to corners, without being corners
    for x_corner, y_corner in domain_points:
        d_corner = np.sqrt((p[:,0]-x_corner)**2 + (p[:,1]-y_corner)**2 )
        mask_almost_corner = np.logical_and(d_corner>0, d_corner<0.1*hmin)
        print("Removing ",sum(mask_almost_corner), "almost corners")
        p = p[np.logical_not(mask_almost_corner)]

    # Compute Delaunay again
    tri = Delaunay(p)
    t = tri.vertices
    Neigh = tri.neighbors
    d = fd(p)
    # Bring to boundary all triangles that are too skew
    mask_bc_cells = (Neigh <0).any(axis=1)
    ix_p = t[Neigh<0]
    bc_points = p[t[mask_bc_cells]]
    bars_1 = np.sqrt( (bc_points[:,0,0]-bc_points[:,1,0])**2 + (bc_points[:,0,1]-bc_points[:,1,1])**2 )
    bars_2 = np.sqrt( (bc_points[:,1,0]-bc_points[:,2,0])**2 + (bc_points[:,1,1]-bc_points[:,2,1])**2 )
    bars_3 = np.sqrt( (bc_points[:,2,0]-bc_points[:,0,0])**2 + (bc_points[:,2,1]-bc_points[:,0,1])**2 )
    bars_mean = (bars_1 + bars_2 + bars_3)/3.
    # Which ones to move?
    ix = ix_p[-d[ix_p]< 0.25 * bars_mean]
    dgradx = (fd(p[ix]+[deps,0])-d[ix])/deps # Numerical
    dgrady = (fd(p[ix]+[0,deps])-d[ix])/deps # gradient
    dgrad2 = dgradx**2 + dgrady**2
    p[ix] -= (d[ix]*np.vstack((dgradx, dgrady))/dgrad2).T # Project
    """

    """
    delta = 10*geps
    ind1 = np.logical_and(xmin - delta < p[:,0], p[:,0] < xmin + delta)
    ind2 = np.logical_and(xmax - delta < p[:,0], p[:,0] < xmax + delta)
    ind3 = np.logical_and(ymin - delta < p[:,1], p[:,1] < ymin + delta)
    ind4 = np.logical_and(ymax - delta < p[:,1], p[:,1] < ymax + delta)
    p[ind1,0] = xmin
    p[ind2,0] = xmax
    p[ind3,1] = ymin
    p[ind4,1] = ymax
    """
    return p, cIter
