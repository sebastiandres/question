from core import Benchmarks

def print_help_message():
    print_general_help_message()
    return

def print_specific_help_message(options):
    # If no help option was selected, print the general help
    if len(options["help"])==0:
        print_general_help_message()
        return

    # Get the wanted help (take lower to be more flexible)
    wanted_help = options["help"][0].lower()
    # Provide help, but just for one case
    if len(options["help"])>1:
        print_general_help_message()
    elif wanted_help=="benchmark":
        print_benchmark_help_message()
    elif wanted_help=="conversion":
        print_conversion_help_message()
    elif wanted_help=="sipat":
        print_sipat_help_message()
    elif wanted_help=="surface":
        print_surface_help_message()
    elif wanted_help=="mesh":
        print_mesh_help_message()
    else: # other option beyond the previous ones.
        print_general_help_message()
    return

def print_benchmark_help_message():
    print("")
    print("Required arguments:")
    print("  --benchmark: ID of the benchmark to be generated.")
    print("  --to:        Path a folder. Data will be created to QAS format.")
    print("")
    print("Optional arguments:")
    print("  --NElems:\tNumber of elements in the mesh.")
    print("  --triangles:\tType of triangle to be used. Choose between: equilateral and rectangular")
    print("  --BC_list:\tBoundary Condition to be applied. Choose 4 among: wall, soft and periodical.")
    print("            \tBC should be provided in order: Left, Right, Bottom, Top.")
    print("  --Bnodes:\tSave bathymetry information at the nodes.")
    print("")
    print("Benchmark IDs:")
    print(Benchmarks.help_str)
    print("")
    print("Example with default settings:")
    print("  question.py --benchmark 8 --to test_benchmark_8")
    print("Example with specific settings:")
    print("question.py --benchmark 8 --NElems 10000 --BC_list wall wall soft soft --to test_benchmark_8")
    print("")
    return

def print_sipat_help_message():
    print("")
    print("Required arguments:")
    print("  --sipat: Path to a sipat scenario in csv format.")
    print("  --to: Path a folder. Data will be created to QAS format.")
    print("")
    print("Optional arguments:")
    print("  --line: (integer) Number of line of file that requires processing (starts in 0).")
    print("  --id: (integer) ID of scenario that requires processing.")
    print("")
    print("Examples:")
    print("question.py --sipat path/to/scenario.csv --to path/to/test_in_qas_format")
    print("question.py --sipat path/to/scenario.csv --to path/to/test_in_qas_format --line 0 3")
    print("question.py --sipat path/to/scenario.csv --to path/to/test_in_qas_format --id 504412")
    print("")
    return


def print_conversion_help_message():
    print("")
    print("Conversion from comcot to SBF:")
    print("Required Arguments:")
    print("  --from_comcot: Path to a folder in comcot format .")
    print("  --to_sbf: Desired path to folder, where data will be converted to SBF format.")
    print("Example:")
    print("  question.py --from_comcot path/to/test_in_comcot_format --to_sbf path/to/test_in_sbf_format")
    print("")
    print("")
    print("Conversion from SBF to QAS:")
    print("Required Arguments:")
    print("  --from_sbf: Path to a folder in comcot format.")
    print("  --to: Desired path to folder, where data will be converted to QAS format.")
    print("Optional arguments:")
    print("  --Bnodes:\tSave bathymetry information at the nodes.")
    print("Example:")
    print("  question.py --from_sbf path/to/test_in_sbf_format --to path/to/test_in_qas_format")
    print("")
    return

def print_surface_help_message():
    print("")
    print("Required arguments:")
    print("  --from: Path to a folder in QAS format .")
    print("  --to:   Path to a folder in QAS format .")
    print("")
    print("Optional arguments:")
    print("  --flat: (W0, U0, V0) Flat surface to be imposed as initial condition.")
    print("  --bell: (X, Y, H, R) Ellipsoid-shaped bump (with no speed) to be imposed as initial condition.")
    print("  --wave: (X, Y, H, W, theta) Solitary wave to be imposed as initial condition.")
    print("  --surface_from_file: (file, coordinate_system) Surface is interpolated from the provided three column file and the coordinate system.")
    print("  --okada_surface: Surface is created using Okada's model with parameters from provided file.")
    print("")
    print("Surface modification example:")
    print("  question.py --from path/to/qas --flat 0 0 0 --to path/to/new_qas")
    print("  question.py --from path/to/qas --bell 0 0 1 1000 --to path/to/new_qas")
    print("  question.py --from path/to/qas --wave 0 0 1 1000 45.0 --to path/to/new_qas")
    print("  question.py --from path/to/qas --surface_from_file path/to/surface_file cartesian --to path/to/new_qas")
    print("  question.py --from path/to/qas --surface_from_file path/to/surface_file geographical --to path/to/new_qas")
    print("  question.py --from path/to/qas --okada_surface path/to/okada_file --to path/to/new_qas")
    return

def print_mesh_help_message():
    print("")
    print("Required arguments:")
    print("  --from_sbf: Path to a folder in comcot format .")
    print("  --to_sbf: Path a folder. Data will be converted to SBF format.")
    print("")
    print("Optional arguments:")
    print("  --adapt: (Niters, dt, hmin) Generates a triangular mesh optimized for ANSWER.")
    print("  --clean: (float) Removes duplicate points.")
    print("  --compress: (Niters, dt, hmin) Generates a triangular mesh optimized for bathymetry.")
    print("  --grid: (dLx, dLy) Generates a regular grid with spacing dLx and dLy.")
    print("")
    print("Mesh modification examples:")
    print("  question.py --from_sbf path/to/sbf --adapt 100 1 1 --to_sbf path/to/mesh_adapted_sbf")
    print("  question.py --from_sbf path/to/sbf --clean 1E-5 --to_sbf path/to/mesh_cleaned_sbf")
    print("  question.py --from_sbf path/to/sbf --compress 100 1 1 --to_sbf path/to/mesh_compressed_sbf")
    print("  question.py --from_sbf path/to/sbf --grid 1E-3 1E-3 --to_sbf path/to/mesh_grid_sbf")
    return

def print_general_help_message():
    print("Please select one of the following to display specific help messages:")
    print("\tHelp on benchmark scenarios usage:")
    print("\t\tquestion.py --help benchmark")
    print("\tHelp on SIPAT usage:")
    print("\t\tquestion.py --help sipat")
    print("\tHelp on converting between formats:")
    print("\t\tquestion.py --help conversion")
    print("\tHelp on modifying the mesh:")
    print("\t\tquestion.py --help mesh")
    print("\tHelp on changing intial surface:")
    print("\t\tquestion.py --help surface")
    return
