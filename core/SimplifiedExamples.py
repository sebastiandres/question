import numpy as np
import os, sys

import Default

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# BATHYMETRY FUNCTIONS
################################################################################
def beach_bathymetry(points, x_left, x_right, z_left, z_right):
    x, y = points[:,0], points[:,1]
    # Bottom
    B = contant_bottom_bathymetry(points, z_left)
    # Height
    B[x>x_right] = z_right
    # Intermediate
    mask = np.logical_and(x_left<=x, x<=x_right)
    B[mask] = z_left + (z_right - z_left)/(x_right-x_left) * (x[mask]-x_left)
    return B

def cliff_bathymetry(points, x_cliff, depth, height):
    B = contant_bottom_bathymetry(points, depth)
    B[points[:,0]>x_cliff] = height
    return B

def contant_bottom_bathymetry(points, depth):
    return depth + np.zeros(points.shape[0])

def random_bottom_bathymetry(points, depth, delta):
    return depth + delta * np.random.random(points.shape[0])

def island_bathymetry(points, xi, yi, z_base, z_height):
    x, y = points[:,0], points[:,1]
    d = ( (x-xi)**2 + (y-yi)**2 )**0.5
    points_d = np.zeros(points.shape)
    dmax  = d.max()
    points_d[:,0] = dmax - d
    B = beach_bathymetry(points_d, 0.80*dmax, 0.90*dmax, z_base, z_height)
    return B

################################################################################
# General creation
################################################################################
def simplified_example(test, Lon_min, Lon_max, Lat_min, Lat_max, ddegres):
    # Create a regular mesh
    lon,lat = np.meshgrid(np.arange(Lon_min,Lon_max,ddegrees),
                          np.arange(Lat_min,Lat_max,ddegrees))
    mesh = np.array([lon.flatten(),lat.flatten()]).T
    # Get the bathymetry
    if test =="beach":
        B = beach_bathymetry(mesh, 0.75*Lon_min+0.25*Lon_max, 0.25*Lon_min+0.75*Lon_max, -6000., 1000)
    elif test =="cliff":
        B = cliff_bathymetry(mesh, 0.5*(Lon_min+Lon_max), -6000., 1000)
    elif test =="constant":
        B = contant_bottom_bathymetry(mesh, -6000.)
    elif test =="irregular":
        B = random_bottom_bathymetry(mesh, -6000., 1000)
    elif test =="island":
        B = island_bathymetry(mesh, 0.5*(Lon_min+Lon_max), 0.5*(Lat_min+Lat_max), -6000., 1000)
    else:
        B = ContantBottom(mesh, -6000.)
    # TS
    dLon = Lon_max - Lon_min
    dLat = Lat_max - Lat_min
    TS = np.array([ [Lon_min+0.5*dLon, Lat_min+0.25*dLat],
                    [Lon_min+0.5*dLon, Lat_min+0.75*dLat]])
    # SP
    NSP = 100
    SP = np.zeros([NSP,2])
    SP[:,0] = np.linspace(Lon_min+0.25*dLon, Lon_min+0.75*dLon, NSP)
    SP[:,1] = Lat_min+0.50*dLat
    # RePack
    sbf_dict = {}
    sbf_dict["Mesh"] = mesh
    sbf_dict["Bathymetry"] = B
    sbf_dict["CoordinateSystem"] = "geographical"
    sbf_dict["LinearizationPoint"] = None
    sbf_dict["TimeSeriesBuoyCoordinates"] = TS
    sbf_dict["SpatialProfileBuoyCoordinates"] = SP
    # Go
    return sbf_dict

if __name__=="__main__":
    import SBFFormat as SBF
    folder = "trash"
    try:
        os.mkdir(folder)
    except:
        pass

    ddegrees = 0.1
    bbox_Iquique = [-74.0, -25.0, -66.0, -15.0] # Iquique
    bbox_Chile = bbox = [-110.0, -60.0, -70.0, -10.0] # Chile
    Lon_min, Lat_min, Lon_max, Lat_max = bbox_Iquique

    tests = ["beach", "cliff", "constant", "irregular", "island"]
    for test in tests:
        # Create test
        sbf_dict  = simplified_example(test, Lon_min, Lon_max, Lat_min, Lat_max, ddegrees)
        # Saving and loading
        path = "{0}/{1}_{2:.2f}".format(folder, test, ddegrees)
        SBF.save(path, sbf_dict)
        sbf_dict2 = SBF.load(path)
        SBF.convert_dict_geographical_to_cartesian(sbf_dict)
        path2 = path + "_cartesian"
        SBF.save(path2, sbf_dict)
        sbf_dict3 = SBF.load(path2)
