import numpy as np
from .. import Common
from .. import LonLatHandler
from .. import Default

from scipy.spatial import Delaunay
#from IPython import embed as ip

def get_raw_values(grid_dic, terminal_BCs):
    """
    This is a very coarse approximation to the Chilean bathymetry.
    """

    # References:
    references = []

    # Suggested parameter usage:
    test_default_BCs = ["soft", "soft", "soft", "soft"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)
    exec_params = {"Tmax":60*60, "g":Default.g(), "manning":0.0}

    # Points that define the continent
    Lon_map = np.array([282.5, 289.5, 286.0, 287.0, 287.0, 296.0, 300.0, 300.0])
    Lat_map = np.array([-10.0, -18.5, -42.0, -42.5, -52.0, -55.0, -38.5, -10.0])

    # Physical Parameters
    depth = 4000.0 # [m]

    # Discretization parameters
    Lon_min =  250.0 # [m] 
    Lon_max =  300.0 # [m]
    Lat_min = -65.0 # [m]
    Lat_max = -10.0 # [m]
    #Lon0, Lat0 = Lon_max, Lat_max
    Lon0, Lat0 = .5*(Lon_min+Lon_max), .5*(Lat_min+Lat_max)

    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = depth
    grid_dic["u_mean"] = (grid_dic["h_mean"]*exec_params["g"])**.5
    # Get the mesh from --NElems, --dL or --dT
    Lon, Lat = Common.get_grid(Lon_min, Lon_max, Lat_min, Lat_max, grid_dic, filename=__file__)
    MeshLonLat = np.array([Lon, Lat]).T
    m2deg = LonLatHandler.generate_meter2lonlat_table(Lon, Lat, Lon0, Lat0)

    ################################################################################
    # Bathymetry (B)
    ################################################################################
    d = Delaunay(np.array([Lon_map, Lat_map]).T)
    dist = -d.plane_distance(MeshLonLat).max(axis=1)
    sigmoid = lambda t : 1./(1.+np.exp(-t))
    B_array = np.where(d.find_simplex(MeshLonLat)>0, depth, -depth)
    BLonLat = np.array([Lon, Lat, B_array]).T

    ################################################################################
    # Water Level Surface (W)
    ################################################################################
    W = np.array([0.0])

    ################################################################################
    # Flux Velocities (U0,V0)
    ################################################################################
    U0 = np.array([0.0])
    V0 = np.array([0.0])

    ################################################################################
    # Required Output
    ################################################################################
    output = {}
    # Movie Animation Step
    output["movie_config"] = {"dt_save":5*60.0, "compute_floading_and_arrival_time":False}
    # Time Series Config and Values
    Lon_map = np.array([282.5, 289.5, 286.0, 287.0, 296.0, 300.0, 300.0])
    Lat_map = np.array([-10.0, -18.5, -42.0, -52.0, -55.0, -38.5, -10.0])

    Lon_TS = Lon_map[1:4] - 1.0
    Lat_TS = Lat_map[1:4]
    output["TS_config"] = [Lon_TS, Lat_TS]
    output["TS_values"] = [np.linspace(0,300), []] # fix
    # Spatial Profile Config and Values
    n_SP = 100
    Lon_SP = np.linspace(Lon_TS.min(), Lon_TS.min(), n_SP)
    Lat_SP = np.linspace(Lat_TS.min(), Lat_TS.max(), n_SP)
    t_SP = [0, 5*60, 10*60]
    output["SP_config"] = [t_SP, Lon_SP, Lat_SP]
    output["SP_values"] = []

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    # If Meter2Degrees  table is provided, it is assumed that the mesh is given in latlon 
    # And the mesh will be transformed (lated and automatically) before saving. 
    return {"Mesh":MeshLonLat, 
            "B":BLonLat, 
            "W":W, 
            "U0":U0, 
            "V0":V0, 
            "Meter2Degrees":m2deg,
            "output":output, 
            "BC_list":test_BC, 
            "references":references, 
            "params":exec_params}
