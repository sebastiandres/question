import numpy as np
from .. import Common
from .. import Default

def get_points(xmin, xmax, ymin, ymax):
    """
    Points for a rectangular mesh
    """
    p = np.array([
              [0.00, 0.00],
              [0.00, 3.00],
              [1.50, 0.00],
              [1.50, 1.50],
              [1.50, 3.00],
              [2.25, 0.75],
              [2.25, 2.25],
              [3.00, 0.00],
              [3.00, 1.50],
              [3.00, 3.00],
                ])
    p[:,0] = xmin + (xmax-xmin)*p[:,0]/3.
    p[:,1] = ymin + (ymax-ymin)*p[:,1]/3.
    return p

def debug_solution(x,y,t):
      """
      Computes the solution for debug case
      """
      return 0.0

def get_raw_values(grid_dic, terminal_BCs):
    """
    Small domain with regular mesh
    """
    # References:
    references = []

    # Suggested parameter usage:
    #test_default_BCs = ["soft", "soft", "periodic", "periodic"] 
    test_default_BCs = ["soft", "soft", "soft", "soft"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)
    exec_params = {"Tmax":50.0, "g":Default.g(), "manning":0.0}

    # Physical Parameters
    d    = 10.0 # [m]

    # Discretization parameters
    xmin = -100.0 # [m] 
    xmax =  100.0 # [m]
    ymin = -100.0 # [m]
    ymax =  100.0 # [m]

    ################################################################################
    # Mesh
    ################################################################################
    if grid_dic["dL"]:
        print "\nWARNING!!!: Cannot override options in this test.\n\t--dL option not applied.\n"
    if grid_dic["dT"]:
        print "\nWARNING!!!: Cannot override options in this test.\n\t--dT option not applied.\n"
    if grid_dic["NElems"]:
        print "\nWARNING!!!: Cannot override options in this test.\n\t--NElems option not applied.\n"
    Mesh = get_points(xmin, xmax, ymin, ymax)

    ################################################################################
    # Bathymetry (B)
    ################################################################################
    x, y = Mesh[:,0], Mesh[:,1]
    z = -d + (x+y)/(x.ptp()+y.ptp())
    B = np.array([x,y,z]).T

    ################################################################################
    # Water Level Surface (W)
    ################################################################################
    z = 0*(-x+-y)/(x.ptp()+y.ptp())
    W = np.array([x,y,z]).T

    #W = np.array([0.0])

    ################################################################################
    # Flux Velocities (U0,V0)
    ################################################################################
    u0 = 0*x
    U0 = np.array([x,y,u0]).T
    v0 = 0*y
    V0 = np.array([x,y,v0]).T


    ################################################################################
    # Required Output
    ################################################################################
    vectorized_eval = np.vectorize(debug_solution)
    output = {}
    # Movie Animation Step
    output["movie_config"] = {"dt_save":1.0, "compute_floading_and_arrival_time":True}
    # Time Series Config
    x_TS = [-85, -45, 10, 0]
    y_TS = [-85, -55, 10, 0]
    output["TS_config"] = [x_TS, y_TS]
    t_TS = np.linspace(0, exec_params["Tmax"], 101)
    # Time Series Known Values
    output["TS_values"] = []
    for (x,y) in zip(x_TS, y_TS):
        output["TS_values"].append(t_TS)
        output["TS_values"].append(vectorized_eval(x, y, t_TS)) 
    # Spatial Profile Config
    N_SP = 21
    t_SP = [0, 2.0, 8.0, 15.0]
    x_SP  = np.linspace(xmin, xmax, N_SP)
    y_SP  = np.zeros(N_SP)
    output["SP_config"] = [t_SP, x_SP, y_SP]
    # Spatial Profile Values
    SP_values = []
    for t in t_SP:
        SP_values.append(vectorized_eval(x_SP, y_SP, t)) 
    output["SP_values"] = [t_SP, SP_values]

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, 
            "output":output, 
            "BC_list":test_BC, 
            "references":references, 
            "params":exec_params}
