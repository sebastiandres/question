import numpy as np
from .. import Common
from .. import Default

def time_serie_values(j):
    """
    Reads the analytical solution for the spatial profile
    """
    Dir = ".".join(__file__.split(".")[:-1]) + ".txt"
    Vals = np.loadtxt(Dir,dtype = float)

    t = Vals[:,0] - 1.5*(j == 0)
    eta = Vals[:,j+1]

    return t, eta

def spatial_profile_values(x, y, t):
    """
    Computes the analytical solution for the dam break
    """
    return np.nan

def get_raw_values(grid_dic, terminal_BCs):
    """
    This laboratory case attempts to simulate the evolution of a incoming wave that pass through different slopes. Moreover, the wave 
    is expected to be fully reflected due to the wall condition at the end of the channel.
    """
    ################################################################################
    # References
    ################################################################################
    references = ["Sinolakis C., Bernard E., Titov V., Kanoglu U.,'Validation and Verification of Tsunami Numerical Models'," +
                  "Pure applied geophys 165 (2008) 2197-2228", "Lubin P., Lemonnier  H.,'Propagation of solitary wave in" + 
                  "constant depth over horizontal beds',Multiphase Science and Technology (16) 239-250, 2004"]

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "wall", "periodic", "periodic"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params =  {"Tmax":30.0, "g":Default.g(), "manning":0.000, "tol_dry":Default.tol_dry()}

 
    # Convenient wrapper for the sech
    sech = lambda x: 1./np.cosh(x)

    # Suggested Model Parameters
    Xo   =  19.85  # [m]
    X1   =   5.00  # [m] 
    g    =  9.810  # [m]
    d    =  0.218  # [m]
    H    =  0.0082  # [m]

    # Discretization parameters
    xmin =   0.00  # [m] 
    xmax =  23.23  # [m]
    ymin =  -2.50  # [m]
    ymax =   2.50  # [m]

    # Related variables
    gamma = np.sqrt(0.75 * H/d**3) # [1/m]
  
    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = d
    grid_dic["u_mean"] = (grid_dic["h_mean"]*grid_dic["g"])**.5

    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    ################################################################################
    # Bathymetry: (B)
    ################################################################################
    Z1 =  1/53.0*(x - 15.04)*(15.04 <= x)*(x < 19.40) + 4.36/53.0*(19.40 <= x)
    Z1 = 1/150.0*(x - 19.40)*(19.40 <= x)*(x < 22.33) + 2.93/150.*(22.33 <= x) + Z1
    Z1 =  1/13.0*(x - 22.33)*(22.33 <= x)*(x < 23.33) + Z1
    Z1 = Z1 - d  
    B  = np.array([x,y,Z1]).T

    ################################################################################
    # Water Level Surface: (W)
    ################################################################################
    Z2 = H*sech(gamma*(x - X1))**2;
    W  = np.array([x,y,Z2]).T

    ################################################################################
    # Flux Velocities: (U0,V0)
    ################################################################################
    u0 = np.sqrt(g/d)*Z2*(1.00 - 0.25*Z2/d)
    U0 = np.array([x,y,u0]).T
    V0 = np.array([0.0])

    # Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}
  
    ################################################################################
    # Required Output
    ################################################################################
    vectorized_eval = np.vectorize(spatial_profile_values)

    # Movie Animation Step
    movie = {"compute":False, "dt_save":1.0}

    # inundation map, heat map and arrival times maps
    inundation   = {"compute":False}
    heatmap      = {"compute":False}
    arrival_time = {"compute":False,"height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Configiguration
    TS = {"compute":True}
    N_TS = 201
    x_TS = [12.64, 17.22, 19.40, 20.86, 22.33, 22.80]
    y_TS = [0,]*len(x_TS)
    t_TS = np.linspace(0, exec_params["Tmax"], N_TS)
    TS["points"] = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/N_TS

    # Time Series Known Values
    TS_values = []
    for j in range(len(x_TS)):
        t_TS, w_TS = time_serie_values(j)
        TS_values.append((t_TS,w_TS))
    TS["measured_values"] = TS_values

    # Spatial Profile Config
    SP = {"compute":True}
    N_SP = 201 
    t_SP = [0, 5, 10, 20, 30]
    x_SP = np.linspace(xmin, xmax, N_SP)
    y_SP = np.zeros(N_SP)
    SP["points"] = list(zip(x_SP,y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    for t in t_SP:
        SP_values.append(vectorized_eval(x_SP, y_SP, t))
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
