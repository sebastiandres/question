import numpy as np
from .. import Common
from .. import Default

def time_serie_values(j):
    """
    Reads the analytical solution for the spatial profile
    """
    Dir = ".".join(__file__.split(".")[:-1]) + ".txt"
    Vals = np.loadtxt(Dir,dtype = float)

    t = Vals[:,0] - 26.50*(j < 4) - 27.00*(3 < j)
    eta = Vals[:,j+1]

    return t, eta

def spatial_profile_values(x, y, t):
    """
    Computes the analytical solution for the dam break
    """
    return np.nan

def get_raw_values(grid_dic, terminal_BCs):
    """
    This laboratory case attempts to assess the runup on a conical island. The aim of this case is first, to test the dry and wet
    condition on the island and second, to study the time series developed in different locations around the island.
    """
    ################################################################################
    # References
    ################################################################################
    references = ["Synolakis C., Bernard E., Titov V., Kanoglu U.,'Validation and Verification of Tsunami Numerical Models'," +
                "Pure applied geophys 165 (2008) 2197-2228"]

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "soft", "periodic", "periodic"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params =  {"Tmax":55.0, "g":Default.g(), "manning":0.0, "tol_dry":Default.tol_dry()}


    # Convenient wrapper for the sech
    sech = lambda x: 1./np.cosh(x)

    # Suggested Model Parameters
    g      = exec_params["g"] # [m/s2]
    water_depth       = 0.320 # [m]
    tank_width        = 35.00 # [m]
    tank_length       = 12.50 # [m]
    island_height     = 0.625 # [m]
    island_toe_diam   = 7.200 # [m]
    island_crest_diam = 2.200 # [m]

    # Water level Parameters
    H  =   0.0144 # [m]
    X1 = -10.960  # [m]

    # Discretization parameters
    xmin = -tank_width /2.0 # [m]
    xmax =  tank_width /2.0 # [m]
    ymin = -tank_length/2.0 # [m]
    ymax =  tank_length/2.0 # [m]

    # Related variables
    gamma = np.sqrt(0.75 * H/water_depth**3) # [1/m]

    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = water_depth
    grid_dic["u_mean"] = (grid_dic["h_mean"]*grid_dic["g"])**.5

    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    ################################################################################
    # Bathymetry: (B)
    ################################################################################
    R  = np.sqrt(x**2 + y**2)
    Z = 0.25*(island_toe_diam/2.0 - R)*(island_crest_diam/2.0 < R)*(R < island_toe_diam/2.0)
    Z = Z + island_height*(R <= island_crest_diam/2.0)
    Z = Z - water_depth
    B  = np.array([x, y, Z]).T

    ################################################################################
    # Water Level Surface: (W)
    ################################################################################
    Z = H*sech(gamma*(x - X1))**2;
    W = np.array([x, y, Z]).T

    ################################################################################
    # Flux Velocities: (U0,V0)
    ################################################################################
    u0 = np.sqrt(g/water_depth)*Z*(1.00 - 0.25*Z/water_depth)
    U0 = np.array([x, y, u0]).T
    V0 = np.array([0.0])

    # Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}

    ################################################################################
    # Required Output
    ################################################################################
    vectorized_eval = np.vectorize(spatial_profile_values)

    # Movie Animation Step
    movie = {"compute":True, "dt_save":1.0}

    # inundation map, heat map and arrival times maps
    inundation   = {"compute":True}
    heatmap      = {"compute":True}
    arrival_time = {"compute":True,"height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Configuration
    N_TS = 201
    TS   = {"compute":True}
    qp   = np.array([[-6.4800,2.2500],[-6.4800,0.2500],[-6.4800,-0.7500],[-6.4800,-2.2500],
                     [-3.6000,0.0000],[-2.6000,0.0000],[ 0.0000,-2.6000],[ 2.6000, 0.0000]])
    x_TS = qp[:,0]
    y_TS = qp[:,1]

    t_TS = np.linspace(0, exec_params["Tmax"], N_TS)
    TS["points"] = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/N_TS

    # Time Series Known Values
    TS_values = []
    for j in range(len(x_TS)):
        t_TS, w_TS = time_serie_values(j)
        TS_values.append((t_TS,w_TS))
        #TS_values.append(w_TS)
    TS["measured_values"] = TS_values

    # Spatial Profile Values
    SP   = {"compute":True}
    N_SP = 201
    island_slope = (island_toe_diam-island_crest_diam)/island_height
    delta_visualization = grid_dic["dL"]
    island_water_level_diam = island_crest_diam + island_slope*(island_height-water_depth) + delta_visualization
    th = np.linspace(0,2*np.pi, N_SP)
    x_SP = 0.5*island_water_level_diam * np.cos(th)
    y_SP = 0.5*island_water_level_diam * np.sin(th)
    t_SP = np.linspace(0., exec_params["Tmax"], 5)
    SP["points"] = list(zip(x_SP,y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    for t in t_SP:
        SP_values.append(vectorized_eval(x_SP, y_SP, t))
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
