import numpy as np
from .. import Common
from .. import Default

def time_serie_values(x, y, t):
    """
    Computes the analytical solution for the dam break
    """
    return np.nan

def spatial_profile_values(j):
    """
    Reads the analytical solution for the spatial profile
    """
    Dir = ".".join(__file__.split(".")[:-1]) + ".txt"
    Vals = np.loadtxt(Dir,dtype = float)

    x = Vals[:,0]
    eta = Vals[:,j+1]
    return x, eta

def get_raw_values(grid_dic, terminal_BCs):
    """
    This laboratory case attempts to assess the runup of a soliton on a steep sloping beach. The wave is expected to runup over 
    the beach without breaking. Experimental data of the water profile as well as time series were taken from the NOOA. 
    """
    ################################################################################
    # References
    ################################################################################
    references = ["Synolakis C. E., 'The Runup of Long Waves', Ph.D. Thesis, California Institute of Technology, pp. 121-128, 1986",
                  "Lubin P., Lemonnier  H.,'Propagation of solitary wave in constant depth over horizontal beds',Multiphase" +
                  "Science and Technology (16) 239-250, 2004"]

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "soft", "periodic", "periodic"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params =  {"Tmax":45.0, "g":Default.g(), "manning":0.000, "tol_dry":Default.tol_dry()}

    # Convenient wrapper for the sech
    sech = lambda x: 1./np.cosh(x)

   # Suggested Model Parameters  
    Xo   = 19.85   # [m]
    X1   = 32.35   # [m]
    d    = 1.000   # [m]
    H    = 0.300   # [m]
    g    = 9.800   # [m/s2]
  
    # Discretization parameters
    xmin = -10.0   # [m]
    xmax =  70.0   # [m]
    ymin =  -5.0   # [m]
    ymax =   5.0   # [m]

    # Related variables
    gamma = np.sqrt(0.75 * H/d**3) # [1/m]

    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = d
    grid_dic["u_mean"] = (grid_dic["h_mean"]*grid_dic["g"])**.5
    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    ################################################################################
    # Bathymetry: (B)
    ################################################################################
    Z1 = np.where(x <= Xo, -d/Xo*x, -d)
    B  = np.array([x,y,Z1]).T

    ################################################################################
    # Water Level Surface: (W)
    ################################################################################
    Z2 = H*sech(gamma*(x - X1))**2;
    W  = np.array([x,y,Z2]).T

    ################################################################################
    # Flux Velocities: (U0,V0)
    ################################################################################
    u0 =-np.sqrt(g/d)*Z2*(1.00 - 0.25*Z2/d)
    U0 = np.array([x,y,u0]).T
    V0 = np.array([0.0])

    #Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}
  
    ################################################################################
    # Required Output
    ################################################################################
    vectorized_eval = np.vectorize(time_serie_values)

    # Movie Animation Step
    movie = {"compute":False, "dt_save":1.0}

    # inundation map, heat map and arrival times maps
    inundation   = {"compute":False}
    heatmap      = {"compute":False}
    arrival_time = {"compute":False, "height_threshold":Default.arrival_time_height_threshold()}

    #TO DO - Get Values from Synolakys thesis!!!
    #--------------------------------------------------------------------
    # Time Series Configuration
    N_TS = 201
    TS   = {"compute":True}
    x_TS = [-5.25, -2.74,-0.23, 5.35, 16.76, 19.23]
    y_TS = [0,]*len(x_TS)
    t_TS = np.linspace(0, exec_params["Tmax"], N_TS)
    TS["points"]  = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/N_TS

    # Time Series Known Values
    TS_values = []
    for j in range(len(x_TS)):
        w_TS = vectorized_eval(x_TS[j], y_TS[j], t_TS)
        TS_values.append((t_TS, w_TS))
    TS["measured_values"] = TS_values
    #--------------------------------------------------------------------

    # Spatial Profile Configuration
    SP   = {"compute":True}
    #x_SP = x_prof
    Np_SP = 135
    x_SP  = np.linspace(xmin, xmax, Np_SP)
    y_SP = [0,]*len(x_SP)
    t_SP = [20.5, 26.5, 32.5, 39.5]/np.sqrt(g/d)
    SP["points"] = list(zip(x_SP,y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    for j in range(len(t_SP)):
        x_prof, w_prof = spatial_profile_values(j)
        SP_values.append(w_prof)  
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
