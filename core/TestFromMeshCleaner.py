import sys

import ArgumentHandler
import MeshCleaner
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

def generate(options):
    """
    Loads files from SBF folder and averages the coordinates and bathymetries.
    """
    # The only valid arguments (not all simultaneously required)
    arguments = ["from", "to", "clean"]
    options["to"] = options["to"].strip()
    options["from"] = options["from"].strip()
    if not ArgumentHandler.accepted_arguments(options, arguments):
        return sys.exit(-1)

    # Must have the to_folder
    arguments = ["from", "to", "clean"]
    if not ArgumentHandler.required_arguments(options, arguments):
        return sys.exit(-1)

    # Parameters for calling the mesh cleaner
    from_folder = options["from"]
    to_folder = options["to"]
    tol = options["clean"]

    # Everything should be ok now, so run execution
    # Run the test with the given options
    MeshCleaner.generate(from_folder, to_folder, tol)

    Common.print_sbf_usage(to_folder)
    return sys.exit(0)
