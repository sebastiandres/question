import numpy as np
from .. import Common
from .. import Default

def water_level_solution(x, y, t, ho, w, a, b, g, s, tau):
    """
    Computes the analytical water level solution for the damped oscillation basin.
    """
    B  = ho * (x/a)**2
    W  = ho + (a**2*b**2*np.exp(-tau*t))/(8*g**2*ho)*(-s*tau*np.sin(2*s*t) + (tau**2/4 - s**2)*np.cos(2*s*t)) - (b**2*np.exp(-tau*t))/(4*g) - np.exp(-tau*t/2.0)/g*(b*s*np.cos(s*t) + 0.5*tau*b*np.sin(s*t))*x

    return np.maximum(B,W)

def get_raw_values(grid_dic, terminal_BCs):
    """
    DESCRIPTION:
    This analytical case attempts to study the the well-balanced property over a variable topography with dry zones. The objective of the 
    model is to test the ability to preserve stady state.
    """
    ################################################################################
    # References
    ################################################################################
    references = ["Thacker W., 'Some exact solution to the nonlinear shallow water equations'. Journal of Fluid Mechanics (107) 499" +
  		  "-508, 1981"] 

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "soft", "periodic", "periodic"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params =  {"Tmax":35.0, "g":Default.g(), "manning":0.001, "tol_dry":1E-08}

    ################################################################################
    # Parameters
    ################################################################################
    # Model Parameters
    a   = 30.00                      # [m]
    b   =  5.00                      # [m/s]
    ho  = 10.00                      # [m]
    g   = exec_params["g"]           # [m/s2]
    tau = exec_params["manning"]     # [1/s]
    w   = np.sqrt(8.0*ho*g/a**2)     # [rad/s]
    s   = np.sqrt(w**2 - tau**2)/2.0 # [rad/s]

    # Discretization parameters
    xmin = -50.0  # [m]
    xmax =  50.0  # [m]
    ymin = -50.0  # [m]
    ymax =  50.0  # [m]
  
    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = ho # Can be improved
    grid_dic["u_mean"] = (grid_dic["h_mean"]*grid_dic["g"])**.5
    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    ################################################################################
    # Bathymetry: (B)
    ################################################################################
    B  = ho * (x/a)**2
    B  = np.array([x, y, B]).T

    ################################################################################
    # Water Level Surface: (W)
    ################################################################################
    W  = ho + a**2*b**2*(tau**2/4 - s**2)/(8*g**2*ho) - b**2/(4*g) - b*s*x/g
    W  = np.array([x, y, W]).T

    ################################################################################
    # Flux Velocities: (U0,V0)
    ################################################################################
    U0 = np.array([0.0])
    V0 = np.array([0.0])

    # Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}
  
    ################################################################################
    # Required Output
    ################################################################################
    # Movie Animation Step
    movie = {"compute":True, "dt_save":2.00}

    # inundation map, heat map and arrival times maps
    inundation = {"compute":True}
    heatmap    = {"compute":True}
    arrival_time = {"compute":True,"height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Config
    TS = {"compute":True}
    N_TS = 201
    x_TS = [0.0, 0.0]
    y_TS = [-15.0, 15.0]
    t_TS = np.linspace(0, exec_params["Tmax"], N_TS)
    TS["points"] = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/N_TS

    # Time Series Known Values
    TS_values = []
    for j in range(len(x_TS)):
        w_TS = water_level_solution(x_TS[j], y_TS[j], t_TS, ho, w, a, b, g, s, tau)
        TS_values.append( (t_TS, w_TS) )
    TS["measured_values"] = TS_values

    # Spatial Profile Config
    SP = {"compute":True}
    N_SP  = 201
    t_SP  = [0.0, 1.682, 3.364, 5.046, 6.728, 8.41, 10.09, 11.774, 13.456, 20.184, 26.912, 33.64]
    x_SP  = np.linspace(xmin, xmax, N_SP)
    y_SP  = np.zeros(N_SP)
    SP["points"] = list(zip(x_SP, y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    for j in range(len(t_SP)):
        Wj_SP = water_level_solution(x_SP, y_SP, t_SP[j], ho, w, a, b, g, s, tau)
        SP_values.append(Wj_SP)
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
