from scipy.interpolate import InterpolatedUnivariateSpline 
from scipy import special 
import numpy as np
import cmath

from .. import Default
from .. import Common

# Convenient wrapper for the sech
sech = lambda x: np.divide(2.0*np.exp(-1.0*x),1.0 + np.exp(-2.0*x))
csch = lambda x: np.divide(2.0*np.exp(-1.0*x),1.0 - np.exp(-2.0*x))

def time_serie_values(gamma, lamda, sigma, kappa, X1, Xo, g, d, nk, nl, dk, j):
    """
    Computes the analytical solution for the time serie
    """
    alpha = np.pi/(2*gamma)
    Phi   = kappa*csch(alpha*kappa)*np.exp(1j*kappa*X1)
    Delta = special.j0(2*Xo*kappa) - 1j*special.j1(2*Xo*kappa)
    X = np.zeros(nl,complex) 
    Y = np.zeros(nl,complex)
 
    for i in range(nl):
        aux  = np.divide(Phi*special.j0(sigma[j]*kappa*Xo/2)*np.exp(-1j*kappa*Xo*(1-lamda[i]/2)),Delta)
        X[i] =  16*1j/3.0*dk*aux.sum(axis=0)
        Y[i] =      4/3.0*dk*aux.sum(axis=0)

    u   = X.real;
    t   = Xo/d*(X.real - lamda/2.0)/np.sqrt(g/d);
    eta = Y.real - u**2/2.0; 
    ind = np.argsort(t)  

    return t[ind], eta[ind]


def spatial_profile_values(gamma, lamda, sigma, kappa, X1, Xo, d, nk, ns, dk, j):
    """
    Computes the analytical solution for the spatial profile
    """
    alpha = np.pi/(2*gamma)

    Phi   = kappa*csch(alpha*kappa)*np.exp(1j*kappa*X1)
    Delta = special.j0(2*Xo*kappa) - 1j*special.j1(2*Xo*kappa)

    X = np.zeros(ns,complex) 
    Y = np.zeros(ns,complex)
 
    for i in range(ns):
        aux  = np.divide(Phi*special.j0(sigma[i]*kappa*Xo/2)*np.exp(-1j*kappa*Xo*(1-lamda[j]/2)),Delta)
        X[i] =  16*1j/3.0*dk*aux.sum(axis=0)
        Y[i] =      4/3.0*dk*aux.sum(axis=0)

    u   = X.real;
    x   = Xo/d*(sigma**2/16 - Y.real + u**2/2);
    eta = Y.real - u**2/2.0;   
    ind = np.argsort(x)
    return x[ind], eta[ind]


def get_raw_values(grid_dic, terminal_BCs):
    """
    DESCRIPTION:
    Danilo will put a reference here, eventually.
    """
    # References
    references = ["Synolakis C. E., 'The Runup of Long Waves', Ph.D. Thesis, California Institute of Technology, pp. 56-65, 1986"]

    # Suggested parameter usage
    test_default_BCs = ["soft", "soft", "periodic", "periodic"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)
    exec_params =  {"Tmax":75.0, "g":Default.g(), "manning":0.0, "tol_dry":Default.tol_dry()}

    ################################################################################
    # MODEL PARAMETERS
    ################################################################################
    # Suggested Model Parameters
    Xo   = 19.85  # [m]
    X1   = 37.35  # [m]
    d    = 1.00   # [m]
    hmax = 0.0185 # [m]
    g    = exec_params["g"]  # [m]

    # Discretization parameters
    xmin = -10.00 # [m]
    xmax =  70.00 # [m]
    ymin =  -5.00 # [m]
    ymax =   5.00 # [m]

    # Physical parameters for the construction of the wave profile
    gamma = np.sqrt( 0.75 * hmax/d**3) # [1/m]

    # Integration limits
    lamda_inf =  0.0
    lamda_sup = -20.0
    sigma_inf = -1.3
    sigma_sup =  4.2
    kappa_inf = -1.6
    kappa_sup =  1.6

    # Numerical integration variables
    ns    = 297
    nk    = 1066
    nl    = 1904
    dk    = (kappa_sup - kappa_inf)/nk
    alpha = np.pi/(2*gamma)
    lamda = np.linspace(lamda_inf, lamda_sup, nl)  
    sigma = np.linspace(sigma_inf, sigma_sup, ns)
    kappa = np.linspace(kappa_inf, kappa_sup, nk)

    ################################################################################
    # INITIAL CONDITIONS AND BATHYMETRY
    ################################################################################
    # Mesh
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = d
    grid_dic["u_mean"] = (grid_dic["h_mean"]*grid_dic["g"])**.5
    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    # Bathymetry: (B)
    Z1 = np.where(x <= Xo, -d/Xo*x, -d)
    B  = np.array([x,y,Z1]).T

    # Water Level Surface: (W)
    Z =  hmax*sech(gamma*(x - X1))**2; 
    W  =  np.array([x, y, Z]).T

    # Flux Velocities: (U0,V0)
    U = -1.0 * np.sqrt(g/d) * Z * (1.00 - 0.25*Z/d)
    U0 = np.array([x, y, U]).T
    V0 = np.array([0.0])

    #Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}

    ################################################################################
    # OUTPUT
    ################################################################################
    # Movie Animation Step
    movie = {"compute":True, "dt_save":2.0}

    # inundation map, heat map and arrival times maps
    inundation = {"compute":True}
    heatmap = {"compute":True}
    arrival_time = {"compute":True, 
                    "height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Config
    TS = {"compute":True}
    Nt_TS = 201
    x_TS = [0.25, 0.74, 5.10, 9.95, 15.71, 19.85]
    y_TS = [0, 0, 0, 0, 0, 0]
    TS["points"] = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/Nt_TS

    # Time Series Known Values
    t_TS = np.linspace(0, exec_params["Tmax"], Nt_TS)
    j = np.array([46,113,180,223,263,287])
    TS_values = []
    for i in range(len(j)):
        t_TS, w_TS = time_serie_values(gamma,lamda,sigma,kappa,X1,Xo,g,d,nk,nl,dk,j[i])
        TS_values.append( (t_TS, w_TS) )
    TS["measured_values"] = TS_values

    # Spatial Profile Config
    SP = {"compute":True}
    Np_SP = 201 
    t_SP  = [25, 35, 45, 55, 65, 75]/np.sqrt(g/d)
    x_SP  = np.linspace(xmin, xmax, Np_SP)
    y_SP  = np.zeros(Np_SP)
    SP["points"] = list(zip(x_SP,y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    j = np.array([240,336,432,528,624,720]) # index for the correct time series
    for i in range(len(j)):
        x_prof, w_prof = spatial_profile_values(gamma,lamda,sigma,kappa,X1,Xo,d,nk,ns,dk,j[i])
        f        = InterpolatedUnivariateSpline(x_prof, w_prof, k=1)
        val      = f(x_SP).T
        ind      = x_SP < x_prof[0]
        val[ind] = 'nan'
        ind      = x_prof[len(x_prof)-1] < x_SP 
        val[ind] = 'nan'
        SP_values.append(val)
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}
    
    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
