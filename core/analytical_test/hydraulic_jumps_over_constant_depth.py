import numpy as np
from scipy.optimize import fsolve
from .. import Common
from .. import Default

def h(h2,h1,h0,g):
    """
    Computes the non-linear water depth for the hydraulic jump
    """
    return h2 - 0.5*h0*(np.sqrt(1.0 + 8.0*(2.0*h2*(np.sqrt(g*h1) - np.sqrt(g*h2))/(np.sqrt(g*h0)*(h2 - h0)))**2) - 1.0)

def hydraulic_jump_solution(x, y, t, g, h0, h1, h2, B):
    """
    Computes the analytical solution for the dam break
    """
    c1 = np.sqrt(g*h1);
    c2 = np.sqrt(g*h2);
    u2 = 2*(np.sqrt(g*h1) - np.sqrt(g*h2));

    if x <= -c1*t: 
      H = h1
    elif -c1*t < x and x <= (u2 - c2)*t:
      H = (4.0/(9.0*g))*(np.sqrt(g*h1) - x/(2.0*t))**2
    elif (u2 - c2)*t < x and x <= 2*h2*(np.sqrt(g*h1) - np.sqrt(g*h2))*t/(h2 - h0): 
      H = h2
    else:
      H = h0

    return B + H

def get_raw_values(grid_dic, terminal_BCs):
    """
    DESCRIPTION:
    This debugging case represents a flat bathymetry, and a hydraulic jump in the center of the channel. The purpose is to check 
    the wall, periodic and soft boundary conditions. For all three cases a symmetric solution in Y-axis is expected.
    """
    ################################################################################
    # References
    ################################################################################
    references = ["J.J. Stoker, Water Waves: The mathematical theory with applications, John Wiley & Sons, 1992, Pages: 312-314"]

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "soft", "periodic", "periodic"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params =  {"Tmax":15.0, "g":Default.g(), "manning":0.0, "tol_dry":Default.tol_dry()}

    ################################################################################
    # Parameters
    ################################################################################
    # Suggested Model Parameters
    h1   = 2.000            # [m]
    h0   = 1.000            # [m]
    g    = exec_params["g"] # [m/s2]
    h2   = fsolve(h, (h1+h0)/2.0, args=(h1,h0,g), xtol=1e-08)
  
    # Discretization parameters
    xmin = -75.00  # [m]
    xmax =  75.00  # [m]
    ymin =  -7.50  # [m]
    ymax =   7.50  # [m]
  
    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = .5*(h1+h0)
    grid_dic["u_mean"] = (grid_dic["h_mean"]*grid_dic["g"])**.5

    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    ################################################################################
    # Bathymetry: (B)
    ################################################################################
    B = np.array([0.0])

    ################################################################################
    # Water Level Surface: (W)
    ################################################################################
    Z = np.where(x <= 0.0, h1, h0)
    W = np.array([x, y, Z]).T

    ################################################################################
    # Flux Velocities: (U0,V0)
    ################################################################################
    U0 = np.array([0.0])
    V0 = np.array([0.0])

    # Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}
  
    ################################################################################
    # Required Output
    ################################################################################
    vectorized_eval = np.vectorize(hydraulic_jump_solution)

    # Movie Animation Step
    movie = {"compute":True, "dt_save":0.50}

    # inundation map, heat map and arrival times maps
    inundation = {"compute":False}
    heatmap    = {"compute":False}
    arrival_time = {"compute":False,"height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Configiguration
    N_TS = 201
    TS   = {"compute":True}
    x_TS = [-50, -10, 10, 50]
    y_TS = [0, 0, 0, 0]
    t_TS = np.linspace(0, exec_params["Tmax"], N_TS)
    TS["points"]  = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/N_TS

    # Time Series Known Values
    TS_values = []
    for j in range(len(x_TS)):
        w_TS = vectorized_eval(x_TS[j], y_TS[j], t_TS, g, h0, h1, h2, 0.0)
        TS_values.append( (t_TS, w_TS) )
    TS["measured_values"] = TS_values

    # Spatial Profile Configiguration
    SP = {"compute":True}
    N_SP  = 201
    t_SP  = [0, 1, 2, 5, 7, 10, 15]
    x_SP  = np.linspace(xmin, xmax, N_SP)
    y_SP  = np.zeros(N_SP)
    SP["points"] = list(zip(x_SP, y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    for j in range(len(t_SP)):
        Wj_SP = vectorized_eval(x_SP, y_SP, t_SP[j], g, h0, h1, h2, 0.0)
        SP_values.append(Wj_SP)
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
