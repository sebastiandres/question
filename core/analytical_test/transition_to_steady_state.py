import numpy as np
from .. import Common
from .. import Default

def solution(x, y, t):
    """
    It does not computes the analytical solution for accuracy test.
    """
    return np.nan

def get_raw_values(grid_dic, terminal_BCs):
    """
    This laboratory case converges to a steady state at 0.07 seconds (?).
    """
    ################################################################################
    # References
    ################################################################################
    references = ["Bryson S., " +
                  "Well-Balanced Positivity Preserving Central-Upwind Scheme on Triangular Grids for the Saint-Venant System, " + \
		  "Mathematical Modelling and Numerical Analysis, 45 (2011), 423-446."]

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "soft", "soft", "soft"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params =  {"Tmax":0.1, "g":1.0, "manning":0.00, "tol_dry":Default.tol_dry()}
 
    # Suggested Model Parameters  
    w0     = 1.000    # [m]
    u0, v0 = 0.3, 0.0 # [m/s]
    # Discretization parameters
    xmin =   0.0   # [m]
    xmax =   2.0   # [m]
    ymin =   0.0   # [m]
    ymax =   1.0   # [m]

    ################################################################################
    # INITIAL CONDITIONS AND BATHYMETRY
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = w0
    grid_dic["u_mean"] = (u0**2 + v0**2)**.5

    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    # Bathymetry: (B)
    Z1 = 0.5*np.exp( -25.*(x-1.)**2 - 50.*(y-0.5)**2)
    B = np.array([x, y, Z1]).T

    # Water Level Surface: (W)
    W  = np.array([w0])

    # Flux Velocities: (U0,V0)
    U0 = np.array([u0])
    V0 = np.array([v0])

    # Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}
  
    ################################################################################
    # Required Output
    ################################################################################
    vectorized_eval = np.vectorize(solution)

    # Movie Animation Step
    movie = {"compute":True, "dt_save":0.01}

    # inundation map, heat map and arrival times maps
    inundation   = {"compute":False}
    heatmap      = {"compute":False}
    arrival_time = {"compute":False,
                    "height_threshold":Default.arrival_time_height_threshold()}


    # Time Series Configuration
    TS   = {"compute":True}
    N_TS = 201
    x_TS = [0.2, 0.5, 0.8, 1, 1.2, 1.5, 1.8]
    y_TS = [0.5,]*len(x_TS)
    t_TS = np.linspace(0, exec_params["Tmax"], N_TS)
    TS["points"]  = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/N_TS

    # Time Series Known Values
    TS_values = []
    for j in range(len(x_TS)):
        w_TS = vectorized_eval(x_TS[j], y_TS[j], t_TS)
        TS_values.append( (t_TS, w_TS) )
    TS["measured_values"] = TS_values


    # Spatial Profile Configuration
    SP   = {"compute":True}
    N_SP = 201
    t_SP = [0, 5, 10, 20, 40, 65]
    x_SP = np.linspace(xmin, xmax, N_SP)
    y_SP = .5*(ymin+ymax) + np.zeros(N_SP)
    SP["points"] = list(zip(x_SP, y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    for j in range(len(t_SP)):
        Wj_SP = vectorized_eval(x_SP, y_SP, t_SP[j])
        SP_values.append(Wj_SP)
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}

