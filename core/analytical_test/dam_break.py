import numpy as np
from .. import Common
from .. import Default

def dam_break_solution(x, y, t, g, H_l, H_r, B):
    """
    Computes the analytical solution for the dam break
    """
    c0 = np.sqrt(g*H_l)
    if x<=-c0*t:
        H = H_l
    elif x>2*c0*t:
        H = H_r
    else:
        H = ((x/t-2*c0)**2)/(9*g)
    return B + H

def get_raw_values(grid_dic, terminal_BCs):
    """
    This analytical case attempts to simulate the break of a simple dam. For a given water height of the dam there is an
    analytic solution  which  is expressed by a second order polynomial function. Therefore a parabolic solution is expected
    to occur between both water levels.
    """
    ################################################################################
    # References
    ################################################################################
    references = ["J.J. Stoker, Water Waves: The mathematical theory with applications, John Wiley & Sons, 1992, Pages: 312-314"]

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "soft", "periodic", "periodic"]
    test_BC = Common.assign_BC(terminal_BCs, test_default_BCs)

    exec_params =  {"Tmax":10.0, "g":Default.g(), "manning":0.0, "tol_dry":1E-08}

    # Suggested Model Parameters
    h = 1.000            # [m]
    g = exec_params["g"] # [m/s2]

    # Discretization parameters
    xmin = -75.00  # [m]
    xmax =  75.00  # [m]
    ymin =  -7.50  # [m]
    ymax =   7.50  # [m]

    ################################################################################
    # Mesh
    ################################################################################
    grid_dic["g"] = exec_params["g"]
    grid_dic["h_mean"] = h
    grid_dic["u_mean"] = (h*g)**.5

    # Get the mesh from --NElems, --dL or --dT
    x, y = Common.get_grid(xmin, xmax, ymin, ymax, grid_dic, filename=__file__)
    Mesh = np.array([x, y]).T

    ################################################################################
    # Bathymetry: (B)
    ################################################################################
    B = np.array([0.0])

    ################################################################################
    # Water Level Surface: (W)
    ################################################################################
    Z = np.where(x <= 0.0, h, 0.0)
    W = np.array([x, y, Z]).T

    ################################################################################
    # Flux Velocities: (U0,V0)
    ################################################################################
    U0 = np.array([0.0])
    V0 = np.array([0.0])

    # Pack
    initial_values = {"Mesh":Mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}

    ################################################################################
    # Required Output
    ################################################################################
    vectorized_eval = np.vectorize(dam_break_solution)

    # Movie Animation Step
    movie = {"compute":True, "dt_save":0.50}

    # inundation map, heat map and arrival times maps
    inundation   = {"compute":False}
    heatmap      = {"compute":False}
    arrival_time = {"compute":False,"height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Configuration
    N_TS = 201
    TS   = {"compute":True}
    x_TS = [-50, -10, 10, 50]
    y_TS = [0, 0, 0, 0]
    t_TS = np.linspace(0, exec_params["Tmax"], N_TS)
    TS["points"]  = list(zip(x_TS, y_TS))
    TS["dt_save"] = exec_params["Tmax"]/N_TS

    # Time Series Known Values
    TS_values = []
    for j in range(len(x_TS)):
        w_TS = vectorized_eval(x_TS[j], y_TS[j], t_TS, g, h, 0, 0.0)
        TS_values.append((t_TS, w_TS))
    TS["measured_values"] = TS_values

    # Spatial Profile Configuration
    N_SP = 201
    SP   = {"compute":True}
    t_SP = [0, 1, 2, 5, 7, 10]
    x_SP = np.linspace(xmin, xmax, N_SP)
    y_SP = np.zeros(N_SP)
    SP["points"] = list(zip(x_SP, y_SP))
    SP["sampling_times"] = t_SP

    # Spatial Profile Values
    SP_values = []
    for j in range(len(t_SP)):
        Wj_SP = vectorized_eval(x_SP, y_SP, t_SP[j], g, h, 0, 0.0)
        SP_values.append(Wj_SP)
    SP["measured_values"] = {"sampling_times":t_SP, "Wj":SP_values}

    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"initial_values":initial_values,
            "execution_parameters":exec_params,
            "references":references,
            "movie":movie,
            "inundation":inundation,
            "heatmap":heatmap,
            "arrival_time":arrival_time,
            "time_series":TS,
            "spatial_profile":SP}
