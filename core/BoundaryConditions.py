import numpy as np
from operator import itemgetter

import Default

if Default.DEBUG_MODE:
    from IPython import embed

###############################################################################
# Empty dictionary for GhostCells and Ghost Cells Neighbors
###############################################################################
def GhostCell_dic():
    return {"left":[], "right":[], "bottom":[], "top":[]}

def configure(tri, BC_list):
    """
    @brief Configures the boundary conditions on the mesh
    Configures the Boundary Conditions on the mesh, depending on the selected Bounary conditions.
    @param tri scipy.spatial.Delaunay triangulation of the mesh
    @param BC_list Array of boundary condition list
    @return coords, elements, neighbors, ghostcells
    """
    return _configure_with_bc_list(tri, BC_list)

###############################################################################
# Configure the elements using the BC list
###############################################################################
def _configure_with_bc_list(tri, BC_list):
    """
    Configures the BC using the options provided on BC_list
    @param tri scipy.spatial.Delaunay triangulation of the mesh
    @param BC_list Array of boundary condition list
    @return coords, elements, neighbors, ghostcells
    """
    tri, GC = _configure_periodical_bc(tri, BC_list)
    # Configure Ghost Cells, if required
    coords, ENo, ENe, GC = _configure_ghost_cells(tri, BC_list)
    # Goes back to SF's favorite edge numbering scheme
    ENe=ENe[:,[2,0,1]]
    return coords.T, ENo.T, ENe.T, GC

###############################################################################
# Gets the list of nodes on the boundary.
# Helper for periodical BCs and ghost cells.
###############################################################################
def _get_domain_boundary(ENe):
    """
    Gets the domain boundary
    @param ENe number_of_elements by number_of_edges_by_element (e.g ENe.shape == (200,3)) array of element neighbors
    @return number_of_boundary_edges by 2 array with element index in the column 0 and egde number in column 1
    """
    return np.array(np.where(ENe < 0)).T


###############################################################################
# Sort list by list, helper for periodical bc
###############################################################################
def _sort_list_by_list(l1, l2, keys):
    """
    Sorts both lists using the values in l2, returning sorted l1 and l2
    """
    l1_sorted, l2_sorted, keys_sorted = [list(x) for x in zip(*sorted(zip(l1, l2, keys), key=itemgetter(-1)))]
    return l1_sorted, l2_sorted, keys_sorted


def _check_edge_direction(ncoord0,ncoord1):
#return 1 if vertical, 0 if horizontal
    if(np.abs(ncoord0[0]-ncoord1[0])<np.abs(ncoord0[1]-ncoord1[1])):
        return 1
    else:
        return 0

###############################################################################
# Periodical Boundary Condition
###############################################################################
def _configure_periodical_bc(tri, BC_list):
    """
    Joins the periodic boundaries setting neighbor for boundary simplices.
    @param tri scipy.spatial.Delaunay triangulation of the mesh
    @param BC_list Array of boundary condition list
    @return tri with modified neighbors, ghostcells
    """
    coords = tri.points
    ENo  = tri.simplices
    ENe  = tri.neighbors
    # Check if anything to do
    connectLeftRight = (BC_list[0]=="periodic") and (BC_list[1]=="periodic")
    connectTopBottom = (BC_list[2]=="periodic") and (BC_list[3]=="periodic")
    # Apply periodic BC, if any
    if not (connectLeftRight or connectTopBottom):
        return tri, []
    # Else, do work
    # Compute the bounding box
    x = coords[:,0]
    y = coords[:,1]
    xmin, xmax = x.min(), x.max()
    ymin, ymax = y.min(), y.max()
    # Go over the elements, and when a boundary element is found, process it
    DomainBoundary = _get_domain_boundary(ENe)
    # Empty containers for top, bottom, left or right lists of element
    U_list, U_sides, U_vals = [], [], []
    D_list, D_sides, D_vals = [], [], []
    L_list, L_sides, L_vals = [], [], []
    R_list, R_sides, R_vals = [], [], []

    # Assign the elements to top, bottom, left or right lists
    for [t,s] in DomainBoundary:
        nindex0 = ENo[t,(s-1)%3]
        nindex1 = ENo[t,(s+1)%3]
        ncoord0 = coords[nindex0,:]
        ncoord1 = coords[nindex1,:]
        edge_centroid=0.5*(ncoord0+ncoord1)
        if (_check_edge_direction(ncoord0,ncoord1)==1):
        #vertical
            if np.abs(xmin-edge_centroid[0])<np.abs(xmax-edge_centroid[0]):
            #closer to the left
                L_list.append(t)
                L_sides.append(s)
                L_vals.append(edge_centroid[1])
            else:
            #closer to the right
                R_list.append(t)
                R_sides.append(s)
                R_vals.append(edge_centroid[1])
        else:
        #horizontal
            if np.abs(ymin-edge_centroid[1])<np.abs(ymax-edge_centroid[1]):
            #closer to the bottom
                D_list.append(t)
                D_sides.append(s)
                D_vals.append(edge_centroid[0])
            else:
            #closer to the top
                U_list.append(t)
                U_sides.append(s)
                U_vals.append(edge_centroid[0])
    # Sort the elements by centroid
    U_list, U_sides, U_vals = _sort_list_by_list(U_list, U_sides, U_vals)
    D_list, D_sides, D_vals = _sort_list_by_list(D_list, D_sides, D_vals)
    L_list, L_sides, L_vals = _sort_list_by_list(L_list, L_sides, L_vals)
    R_list, R_sides, R_vals = _sort_list_by_list(R_list, R_sides, R_vals)
    # Check values match if they are required to be connected
    if connectLeftRight:
        if len(L_vals)!=len(R_vals) or not np.allclose(L_vals, R_vals):
            print "Number of left and right elements do not match"
            print "Left/Right periodic BC not applied"
            return tri, np.array([])
    if connectTopBottom:
        if len(U_vals)!=len(U_vals) or not np.allclose(U_vals, D_vals):
            print "Number of top and bottom elements do not match"
            print "Top/Bottom periodic BC not applied"
            return tri, np.array([])
    # Rewire the corresponding neighbors, if required
    if connectLeftRight:
        for t_l, s_l, t_r, s_r in zip(L_list, L_sides, R_list, R_sides):
            ENe[t_l,s_l] = t_r
            ENe[t_r, s_r] = t_l
    if connectTopBottom:
        for t_u, s_u, t_d, s_d in zip(U_list, U_sides, D_list, D_sides):
           ENe[t_u,s_u] = t_d
           ENe[t_d,s_d] = t_u
    return tri, np.array([])


###############################################################################
# Assign the coefficient that will produce the desired BC
###############################################################################
def get_BC_index(bc):
    if bc=="soft":
        return +1
    if bc=="wall":
        return -1
    return 0

###############################################################################
# Configure the Ghost Cells for wall or soft boundary conditions
###############################################################################
def _configure_ghost_cells(tri, BC_list):
    """
    Creates the ghost cells modifiying the ElementNodes, ElementNeighbors and NodeCoordenates
    """
    coords = tri.points.copy()
    ENo  = tri.simplices.copy() # Defines a triangle by the nodes
    ENe  = tri.neighbors.copy() # List the neighbors for each element, -1 if none.
    #Change from opposite vertex to adjacent vertex numeration.
    GC_sides = []
    GC_index = []
    for BCi in BC_list:
        GC_sides.append(BCi in ["wall", "soft"])
        GC_index.append(get_BC_index(BCi))
    # Check if anything to do at all
    if not any(GC_sides):
        return coords, ENo, ENe, np.array([])
    # Do the work
    new_coords = []
    new_ENo = []
    new_ENe = []
    GC = GhostCell_dic()
    GCN = GhostCell_dic()
    GC_list = []
    # Get the x and y
    x = coords[:,0]
    y = coords[:,1]
    xmin, xmax = x.min(), x.max()
    ymin, ymax = y.min(), y.max()
    # Go over the elements, and when a boundary element is found, process it
    DomainBoundary = _get_domain_boundary(ENe)
    #Discard corner duplicates, we only need to know it's on the boundary
    #DomainBoundary = np.unique(DomainBoundary[:,0])
    added_cells = ENo.shape[0] # To rewire
    added_nodes = coords.shape[0] # To rewire
    for [t,s] in DomainBoundary:
        nindex0 = ENo[t,(s+1)%3]
        nindex1 = ENo[t,(s-1)%3]
        nindex2 = ENo[t,(s)]
        ncoord0 = coords[nindex0,:]
        ncoord1 = coords[nindex1,:]
        ncoord2 = coords[nindex2,:]
        #calculate edge_centroid
        edge_centroid=0.5*(ncoord0+ncoord1)
        #calculate n2 projection on the line that passes through n0 and n1
        vec2=ncoord2-ncoord0
        vec1=ncoord1-ncoord0
        vecc=(vec2.dot(vec1)/vec1.dot(vec1))*vec1
        vecr=vec2-vecc
        new_coords.append(ncoord2-2*vecr)
        if (_check_edge_direction(ncoord0,ncoord1)==1):
        #vertical
            if np.abs(xmin-edge_centroid[0])<np.abs(xmax-edge_centroid[0]):
            #closer to the left
                GC_list.append([added_cells, t, GC_index[0]])
            else:
            #closer to the right
                GC_list.append([added_cells, t, GC_index[1]])
        else:
        #horizontal
            if np.abs(ymin-edge_centroid[1])<np.abs(ymax-edge_centroid[1]):
            #closer to the bottom
                GC_list.append([added_cells, t, GC_index[2]])
            else:
            #closer to the top
                GC_list.append([added_cells, t, GC_index[3]])
        # Update
        new_ENo.append([nindex1, nindex0, added_nodes])
        new_ENe.append([-1, -1, t])
        ENe[t,s] = added_cells
        added_cells += 1
        added_nodes += 1
    # Rewire the respective matrices
    ENe = np.concatenate([ENe, np.array(new_ENe)], axis=0)
    ENo = np.concatenate([ENo, np.array(new_ENo)], axis=0)
    coords = np.concatenate([coords, np.array(new_coords)], axis=0)

    # Return the GhostCellNeighbors and GhostCells
    return coords, ENo, ENe, np.array(GC_list)
