import numpy as np
import urllib2
import zipfile
import sys
import os

import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

# change local_password periodically, do not use as letters: ' " | ; *
local_password = '''SWE__sd5YmdT2e3rH@x*wewae77jME@E+SW[t8*{KPj__SWE'''

url_dic = {
           #"sipat_mesh": "http://www.inf.utfsm.cl/~qas/sipat_mesh_old.zip",
           #"sipat_mesh": "http://www.inf.utfsm.cl/~qas/sipat_mesh_120000.zip",
           #"sipat_mesh": "http://www.inf.utfsm.cl/~qas/sipat_mesh_1800000.zip",
           "sipat_mesh": "http://www.inf.utfsm.cl/~qas/qas_okada_from_closest_sipat_main_layer_reduce_0.1.zip",
          }

################################################################################
# Get the file if not already there, and unzips it
################################################################################
def download(dest_folder, test):
    # Get the link and the name of the requested file
    url = url_dic[test]
    if url=="":
        print "\tPrecomputed QUESTION files not available (yet)."
        print "\tPlease select another test."
        return sys.exit(-1)

    # Get the correct folders and full paths
    core_path = os.path.dirname(__file__)
    tmp_folder = os.path.join("field_test", "tmp")
    tmp_path = os.path.join(core_path, tmp_folder)
    Common.mkdir(tmp_path) # I need this folder to be at least created to query them later
    unzip_folder = tmp_path
    requested_file = url.split("/")[-1].split("?")[0]
    status_file = requested_file.replace(".zip","_status.txt")
    # Download files only if not present
    all_tmp_files = os.listdir(tmp_path)
    target_folder =  requested_file.replace(".zip","")
    if not (status_file in all_tmp_files and requested_file in all_tmp_files):
        download_data(url, tmp_path, requested_file, status_file)
    if not target_folder in all_tmp_files:
        unzip(os.path.join(tmp_path, requested_file), unzip_folder, local_password)
    # copy to the corresponding folder
    extracted_folder_path = os.path.join(unzip_folder, test)
    dest_folder_abspath = os.path.abspath(dest_folder)
    Common.make_folders(dest_folder_abspath)
    for folder in ["InitialConditions", "Mesh", "Output"]:
        origin = os.path.join(extracted_folder_path, folder, "*")
        destin = os.path.join(dest_folder_abspath, folder)
        order = "cp -r {0} {1}".format(origin, destin)
        os.system(order)
    print("\tCompleted downloading and copying files.")
    return

################################################################################
# Downloads file from the web
################################################################################
def download_data(url, path, zip_filename, status_filename):
    """
    Function used to pretty download the zip file
    """
    file_path = os.path.join(path, zip_filename)
    u = urllib2.urlopen(url)
    # Open the file and save
    f = open(file_path, 'wb')
    meta = u.info()
    file_size = float(meta.getheaders("Content-Length")[0])
    print "Downloading: %s (%.1f Mb)" % (zip_filename, 1E-6*file_size)
    # Initialize download
    file_size_dl = 0
    block_sz = 8192
    while True:
        buffer = u.read(block_sz)
        if not buffer:
            break
        file_size_dl += len(buffer)
        f.write(buffer)
        status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
        status = status + chr(8)*(len(status)+1)
        print status,
    print ""
    f.close()
    # Write a file to disk to acknowledge that everything got download properly
    file_path = os.path.join(path, status_filename)
    with open(file_path, "w") as f2:
        f2.write("Download Completed")
    return

################################################################################
# Unzips a file with password, if provided
################################################################################
def unzip(zipFilePath, extractDir, password=None):
    unzip_status = os.system("which unzip")
    if unzip_status==0:
        unzip_status = fast_unzip(zipFilePath, extractDir, password) # unreliable, unzip might not be installed
    if unzip_status!=0:
        slow_unzip(zipFilePath, extractDir, password) # reliable
    return

def fast_unzip(zipFilePath, extractDir, password=None):
    print "Unzipping with program unzip on system terminal"
    order = 'unzip -q -u -P "%s" "%s" -d "%s"' %(local_password, zipFilePath, extractDir)
    status = os.system(order)
    return status

def slow_unzip(zipFilePath, extractDir, password=None):
    print "Unzippping file using slower python library"
    # Make object
    zip = zipfile.ZipFile(zipFilePath)
    # Add password
    if password:
      zip.setpassword(password)
    # Unzip
    try:
        zip.extractall(path=extractDir)
        print "File has been unzipped."
    except:
        print "File could not be unzipped."
    return
