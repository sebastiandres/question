import numpy as np
import sys

import SBFFormat as SBF

################################################################################
# GENERATES ALL THE REQUIRED MESH PROPERTIES
################################################################################
def generate(from_folder, to_folder, tol):
    # Load the data
    if not SBF.has_valid_format(from_folder):
        print("Mesh Cleaner only loads Simplified Bathymetry Format (SBF) Files.")
        sys.exit(-1)
    else:
        sbf_dict = SBF.load(from_folder)
    # Process the data
    xyb = np.zeros([sbf_dict["Mesh"].shape[0], 3])
    xyb[:,:2] = sbf_dict["Mesh"]
    xyb[:,2] = sbf_dict["Bathymetry"]
    clean(xyb, tol)
    sbf_dict["Mesh"] = xyb[:,:2]
    sbf_dict["Bathymetry"] = xyb[:,2]
    # Save the data
    SBF.save(to_folder, sbf_dict)
    return

def clean(xyb, tol):
    """
    Detects the points close to the tolerance and groups them.
    It then averages the points.
    xyb is processed in place (destroys values).
    """
    N = xyb.shape[0]
    groups = -np.ones(N) # All values are -1 at the start
    # Make the groups
    tol2 = tol**2
    print("Forming groups of close points")
    for i in xrange(N):
        if (i%1000==0):
            print("Processing point {0:,} out of {1:,}".format(i,N))
        # Process
        d2 = (xyb[i:,0]-xyb[i,0])**2 + (xyb[i:,1]-xyb[i,1])**2
        mask = (d2<=tol2)
        # Check if save to previous group or to current group
        if groups[i]<0:
            groups[i:][mask] = i
        else:
            groups[i:][mask] = groups[i]
    # Average results for each group
    print("{0} groups from {1} original points".format(len(set(groups)),N))
    print("Averaging data for each group")
    for j in set(groups):
        xyb[groups==j,:] = xyb[groups==j,:].mean(axis=0)
    # Return the results
    return xyb

if __name__=="__main__":
    mesh = np.array([[0.5, 0.5],
                     [0.51,0.51],
                     [0.51,0.51],
                     [0.251,0.25],
                     [0.251,0.25],
                     [0.25,0.251],
                     [0.49,0.49],
                     ])
    b = np.array([1., 2., 1., 3., 4., 1., 1.])
    tol = 0.1
    xyb = np.zeros([mesh.shape[0], 3])
    xyb[:,:2] = mesh
    xyb[:,2] = b
    clean(xyb, tol)
    print xyb
