import numpy as np
import glob
import os

import Default

if Default.DEBUG_MODE:
    from IPython import embed

'''
WARNING: Do not use this library directly. Use COMCOTFormat instead.
'''

################################################################################
# LOADER FOR COMCOT FILES
################################################################################
def load_as_sbf_dict(comcot_path):
    # Read the values and filenames from comcot main file
    ctl_files = glob.glob((comcot_path + "/*.ctl").replace("//","/"))
    main_ctl = sorted(ctl_files)[0] # At least one, because we checked the format
    comcot_dict = _read_from_ctl(main_ctl)
    # Read the corresponding files
    # We will create/reshape the data, and always take the mesh (node coords)
    # from the bathymetry B. The reason behing this choise: the values for W
    # might not be given and the bathymetry must be preserved more precisely.

    # Read bathymetry
    print("Reading bathymetry file: {0}".format(comcot_dict['B']))
    COMCOT_B = load_B_layers(comcot_path, comcot_dict['B'], comcot_dict['B_extra'])

    # Fix the Bathymetry, to simplifed/qas notation (positive over water)
    COMCOT_B[:,2] *= -1.0
    # The obtained values are
    mesh = COMCOT_B[:,:2]
    B = COMCOT_B[:,2]

    # Read the Time Series, if possible
    TS_file =  os.path.join(comcot_path, "ts_location.dat")
    if os.path.exists(TS_file):
        print("Reading Buoy positions (Time Series) from file: {0}".format("ts_location.dat"))
        TS = np.loadtxt(TS_file)
        print("\t {0} points".format(TS.shape[0]))
    else:
        print("!!! Buoy positions (Time Series) file {0} not found".format("ts_location.dat"))
        print("!!! No buoys (Time Series) will be considered.")
        TS = np.array([]) # Must always return something

    # Read the Time Series, if possible
    SP_file =  os.path.join(comcot_path, "sp_location.dat")
    if os.path.exists(SP_file):
        print("Reading Buoy positions (Spatial Profile) from file: {0}".format("sp_location.dat"))
        SP = np.loadtxt(SP_file)
        print("\t {0} points".format(SP.shape[0]))
    else:
        print("!!! Buoy positions (SpatialProfile) file {0} not found".format("ts_location.dat"))
        print("!!! No buoys (SpatialProfile) will be considered.")
        SP = np.array([]) # Must always return something

    # Return to be saved in specific formats
    return {"Mesh":mesh,
            "Bathymetry":B,
            "TimeSeriesBuoyCoordinates":TS,
            "SpatialProfileBuoyCoordinates":SP,
            "CoordinateSystem":"geographical"}

################################################################################
# LOADS THE BATHYMETRY HANDLING THE EXTRA LAYERS
################################################################################
def load_B_layers(comcot_path, B_file, B_layers):
    """
    Loads all the Layers, deletes duplicated points.
    """
    B_file = os.path.join(comcot_path, B_file)
    COMCOT_B = np.loadtxt(B_file)
    print("\t{0} points".format(COMCOT_B.shape[0]))
    null_layer = "no_data.xyz"
    for B_extra_file in B_layers:
        if B_extra_file!= null_layer:
            B_extra_filepath = os.path.join(comcot_path, B_extra_file)
            if os.path.exists(B_extra_filepath):
                print("Reading additional layer of bathymetry: {0}".format(B_extra_file))
                COMCOT_B_extra = np.loadtxt(B_extra_filepath)
                COMCOT_B = mix_bathymetries(COMCOT_B, COMCOT_B_extra)
            else:
                print("!!! Additional layer of bathymetry {0} mentioned on ctl file but not found on folder.".format(B_extra_file))
    print("Final bathymetry loaded: {0} points".format(COMCOT_B.shape[0]))
    return COMCOT_B

def mix_bathymetries(B_main, B_extra, dtol=1E-8):
    """
    Assumes the B_extra is a finer mesh than B_main.
    It removes the points that would be duplicated
    due to overlapping meshes.
    """
    x, y = B_main[:,0], B_main[:,1]
    x_min, y_min, _ = B_extra.min(axis=0)
    x_max, y_max, _ = B_extra.max(axis=0)
    x_aux = np.unique(B_extra[:,0])
    y_aux = np.unique(B_extra[:,1])
    dx = 0.25*(x_aux[1]-x_aux[0])
    dy = 0.25*(y_aux[1]-y_aux[0])
    # Remove all interior point
    in_x = np.logical_and(x_min-dx<x, x<x_max+dx)
    in_y = np.logical_and(y_min-dy<y, y<y_max+dy)
    mask = np.logical_and(in_x, in_y)
    B_main = B_main[np.logical_not(mask)]
    # Append the new Layer
    B_main = np.vstack([B_main, B_extra])
    return B_main

################################################################################
# READS A COMCOT CTL FILE
################################################################################
def _read_from_ctl(ctl_filepath):
    # Go over the arguments and their cast
    string = lambda x: str(x.strip()) # to better handle string cast
    comcot_translation_dict = {}
    # Add the keys that need to be read
    comcot_translation_dict["Tmax"] = (float,
                                       "Total run time")
    comcot_translation_dict["TSdt"] = (float,
                                       "Time interval to Save Data")
    comcot_translation_dict["Z"] = (string,
                                       "Specify Input Z filename")
    comcot_translation_dict["U"] = (string,
                                       "Specify Input U filename")
    comcot_translation_dict["V"] = (string,
                                       "Specify Input V filename")
    comcot_translation_dict["W0"] = (string,
                                       "File Name of Deformation Data")
    comcot_translation_dict["B"] = (string,
                                       "File Name of Bathymetry Data")
    comcot_translation_dict["B_extra"] = (string,
                                          "FileName of Water depth data")
    comcot_translation_dict["manning"] = (float,
                                          "Manning's Roughness Coef")
    # Read them
    comcot_path = os.path.dirname(ctl_filepath)
    ctl_file = os.path.basename(ctl_filepath)
    d = comcot_dict_reader(comcot_path, ctl_file, comcot_translation_dict)
    return d

################################################################################
# Generic reader that dumps the file into a dict
################################################################################
def comcot_dict_reader(path, filename, comcot_translation_dict):
    file_path = os.path.join(path, filename)
    dd = {}
    with open(file_path, "r") as fh:
        print("Reading main ctl file: {0}".format(filename))
        for line in fh.readlines():
            if line[0]=="#":
                continue # if comment, skip the line
            str_pieces = line.split(":")
            comcot_key_with_trash = str_pieces[0].strip()
            value = str_pieces[-1]
            for key, (cast_function, comcot_key) in comcot_translation_dict.items():
                if comcot_key in comcot_key_with_trash:
                    if key in dd:
                        # Convert to a list, if not already done and append the value
                        if type(dd[key])!=list:
                            dd[key] = [dd[key],]
                        dd[key].append(cast_function(value))
                    else:
                        # Just initialize as a value
                        dd[key] = cast_function(value)
    return dd

################################################################################
# Practical to handle COMCOT trashy strings
################################################################################
def _reverse_find(my_list, element):
    found = [(el in element) for el in my_list]
    if any(found):
       return np.argmax(found)
    else:
       return None
