import numpy as np
import sys

# Set DEBUG_MODE to True to have embed in all files
# Set DEBUG_MODE to False before committing to production
DEBUG_MODE = True

# To avoid errors when calling IPython if not installed
try:
    import IPython
except:
    DEBUG_MODE = False

def max_to_min_edges_ratio():
    """
    Ratio between elements in MeshAdapter
    """
    return 20

def ImplementedBoundaryConditions():
  """
  Valid choices for the boundary condition.
  """
  return["soft", "wall", "periodic", "none"]

def arrival_time_height_threshold():
  """
  Default value height used for computing arrival times
  """
  return 1E-6

def g():
  """
  Gravity constant
  """
  return 9.81


def manning():
  """
  Manning's Roughness coefficient
  """
  return 0.000 #0.001


def tol_dry():
  """
  Tolerance for considering a cell as dry
  """
  return 1.0E-6

def tsunami_simulation_Tmax(nhours=8):
    return nhours*60*60 # in seconds

def Tmax(xj, yj, Bj, Wj, g, tol_dry=tol_dry()):
  """
  Computes an estimation of the required time for a gravity wave to go through
  the domain at least twice
  """
  H = Wj - Bj
  water_height_mean  = H[H>tol_dry].mean()
  xmin, xmax = xj.min(), xj.max()
  ymin, ymax = yj.min(), yj.max()
  v_mean = np.sqrt(g*(water_height_mean))
  Lmax = np.sqrt( (xmax-xmin)**2 + (ymax-ymin)**2 )
  Tmax = Lmax / v_mean
  return Tmax

def poisson_coefficient():
  """
  Default value for Poisson coefficient (which is adimentional)
  """
  poisson_coefficient = 0.25
  return poisson_coefficient
