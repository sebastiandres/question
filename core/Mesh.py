import numpy as np
from scipy import interpolate
from scipy.spatial import Delaunay

import BoundaryConditions
import MeshUtilities
import Default

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# INTERPOLATION WRAPPER
################################################################################
def my_interpolate(new_mesh, old_mesh, method="linear"):
    """
    Wraps scipy's griddata interpolation for convenience.
    If interpolation methods needs to change must change only here.
    old_mesh:  [ [x_1,...,x_n],[y_1,...,y_n],[z_1,..., z_n]]
    new_mesh:  [ [x'_1,...,x'_n],[y'_1,...,y'_n]]
    """
    # If the old_mesh is a single value, then we just need to extend the values.
    old_is_1D = ( len(old_mesh.T)==1 )
    if old_is_1D:
        # Case where old_mesh is just a float
        new_z = old_mesh * np.ones(new_mesh.shape[1])
        return new_z
    # If the (new_mesh) = ( old_mesh + some new points),
    # IE, the points in the intersection are the same, then we can
    # assume the mesh is the same and we just added the ghost cells.
    # We just need to extend the vector to allow for more space
    trash, NElems_old = old_mesh.shape
    trash, NElems_new = new_mesh.shape
    if (NElems_old<=NElems_new):
        new_nodes = new_mesh[:,:NElems_old]
        old_nodes = old_mesh[:2,:]
        if np.allclose(new_nodes, old_nodes):
            #print "Needed extension"
            # We just extend the current container. The values are not important, as they are
            # reset later in the code.
            extended_values = old_mesh[2,:].copy()
            extended_values.resize(NElems_new)
            #print "Done"
            return extended_values
    # It nothing else, we must interpolate
    #print "Needed Interpolation"
    old_xy = old_mesh[:2,:].T
    old_z = old_mesh[2,:].T
    new_xy = new_mesh.T
    new_z = interpolate.griddata(old_xy, old_z, new_xy, method=method)
    #print "Done"
    return new_z.flatten()

################################################################################
# CHECK IF ELEMENTS CORRECTLY ORIENTED
################################################################################
def correctly_oriented(point_1, point_2, point_3):
    """
    Checks if triangles are correctly oriented by checking
    the z component of the cross product of sides as vectors.
    """
    P = np.zeros((3,2))

    # Cross Producto between two vectors:
    P[:2,0] = point_2 - point_1
    P[:2,1] = point_3 - point_1

    return ( np.cross(P[:,0],P[:,1])[2] > 0 )

################################################################################
# FIX ORIENTATION OF ELEMENTS
################################################################################
def fix_orientation(ElemNodes, ElemNeigh, NodeCoords):
    """
    Changes the order of nodes for the element if not correctly oriented.
    It it directly done to the ElemNodes and ElemNeigh, so nothing is returned
    Inputs	 :
        ElemNodes : Conectivity array of Elements,	   [3, NumElem]
        ElemNeigh : Neighbor elements connectivity array,  [3, NumElem]
        NodeCoords: Coordinates array of each node, 	   [2, NumPoints]
    Outputs:
        ElemNodes : Conectivity array of Elements,	   [3, NumElem]
        ElemNeigh : Neighbor elements connectivity array,  [3, NumElem]
        NodeCoords: Coordinates array of each node, 	   [2, NumPoints]
    """
    NumElems = ElemNodes.shape[1]

    for i in xrange(NumElems):
        p0, p1, p2 = ElemNodes[:,i]
        t0, t1, t2 = ElemNeigh[:,i]  #Delaunay weird notation is oposite to vertex

        #If already in good position, check orientation
        point_0 = NodeCoords[:, p0]
        point_1 = NodeCoords[:, p1]
        point_2 = NodeCoords[:, p2]

        if (p0<p1 and p0<p2):
            if correctly_oriented(point_0, point_1, point_2):
                ElemNodes[:,i] = [p0, p1, p2]
                ElemNeigh[:,i] = [t2, t0, t1]
            else:
                ElemNodes[:,i] = [p0, p2, p1]
                ElemNeigh[:,i] = [t1, t0, t2]

        if (p1<p0 and p1<p2):
            if correctly_oriented(point_1, point_0, point_2):
                ElemNodes[:,i] = [p1, p0, p2]
                ElemNeigh[:,i] = [t2, t1, t0]
            else:
                ElemNodes[:,i] = [p1, p2, p0]
                ElemNeigh[:,i] = [t0, t1, t2]

        if (p2<p1 and p2<p0):
            if correctly_oriented(point_2, point_0, point_1):
                ElemNodes[:,i] = [p2, p0, p1]
                ElemNeigh[:,i] = [t1, t2, t0]
            else:
                ElemNodes[:,i] = [p2, p1, p0]
                ElemNeigh[:,i] = [t0, t2, t1]

    return

################################################################################
# GLOBAL INDEXATION FOR NEIGHBOUR ELEMENTS
################################################################################
def global_index_selection(Local, Sides):
    """
    Indexing notation for getting neighboring elements
    from an array (3, nElems)
    """
    #Unpacking
    #Local  = md["ElemNeigh"]
    #Sides  = md["ElemNeighSides"]
    nElems = Local.shape[1] #md["NumElems"]

    col = Local.copy()
    ind = col<0
    aux = np.array([range(nElems),range(nElems),range(nElems)])
    col[ind] = aux[ind]

    row = Sides.copy()
    ind = row<0
    one = np.ones(nElems,dtype=int)
    aux = np.array([0*one,one,2*one])
    row[ind] = aux[ind]

    position = row*nElems + col
    return position

################################################################################
# GET THE NEIGHBORS TO THE ELEMENT IN THE jk FORMAT
################################################################################
def get_neighbor_local_sides(ENo, ENe):
    """
    Returns local indexing of the sides for an element.
    This allows to asily compute the lenghts
    Input    :
	ENe  : Element Neighbors, [3, nElem]
    Output   :
	ENLS : Element Neighbors Local Sides, [3, nElem]
    """
    ENLS = np.zeros(ENo.shape, dtype=int)

    # Define a helper to get the indexing of sides
    def get_side(t_k, ne0, ne1, ne2):
        if t_k==ne0:
            return 0
        elif t_k==ne1:
            return 1
        elif t_k==ne2:
            return 2
        else:
            return -1

    # Now classify each node looking for the neighbors of its neighbors
    for ei, (t0, t1, t2) in enumerate(ENe.T):
        s0 = get_side(ei, *ENe[:, t0])
        s1 = get_side(ei, *ENe[:, t1])
        s2 = get_side(ei, *ENe[:, t2])
        ENLS[:, ei] = [s0, s1, s2]

    return ENLS

################################################################################
# GENERATES ALL THE REQUIRED MESH PROPERTIES
################################################################################
def generate(raw_test_values, saveBnodes, domain_points=None, exclude_points=None):
    """
    Generates the triangulation using the delaunay and produces all the
    properties required to be written to disk
    """
    mesh = raw_test_values["initial_values"]["Mesh"]
    # The triangulation of the given points:
    tri = Delaunay(mesh)
    """
    # Delete points that produce bad triangles
    q, worst_side = MeshUtilities.compute_triangle_quality(tri)
    delete_nodes = list(set(worst_side[q<0.3]))
    print("\tDeleting {0} nodes because of low triangle quality".format(len(delete_nodes)))
    mesh = mesh[np.setdiff1d(np.arange(mesh.shape[0]), delete_nodes),:]
    tri = Delaunay(mesh)
    """

    """
    # We fix here to have L-shaped figures
    if domain_points is not None:
        domain_mask = MeshUtilities.get_mask_for_true_domain(tri, domain_points, exclude_points)
        exterior_mask = np.logical_not(domain_mask)
    else:
        exterior_mask = np.zeros(tri.nsimplex, dtype=bool)

    MeshUtilities.fix_delaunay(tri, exterior_mask)

    # Fix abnormal boundaries that would case artificially small Courant restrinctions
    delete_cells_mask = MeshUtilities.get_mask_for_irregular_cells(tri)
    while delete_cells_mask.sum()!=0:
        MeshUtilities.fix_delaunay(tri, delete_cells_mask)
        delete_cells_mask = MeshUtilities.get_mask_for_irregular_cells(tri)
    """

    # OBS: ElemNeigh as provided is in "oposite to vertex" notation. We'll fix that and the orientation,
    # so our algorithm works the jk notation.
    #fix_orientation(ElemNodes, ElemNeigh, NodeCoords)

    # Add the Ghost Cells or rewire for periodic conditions
    BC_list = raw_test_values["initial_values"]["BC_list"]
    NodeCoords, ElemNodes, ElemNeigh, GC = BoundaryConditions.configure(tri, BC_list)
    ElemNeighSides = get_neighbor_local_sides(ElemNodes, ElemNeigh)
    ElemSides = global_index_selection(ElemNeigh, ElemNeighSides)

    # Print Elements including the Ghost Cells
    print "%d Nodes and %d Elements" %(NodeCoords.shape[1], ElemNodes.shape[1])
    # Value of Bathymetry at the finer mesh points
    B_at_nodes = my_interpolate(NodeCoords, raw_test_values["initial_values"]["B"].T)
    Bjk           = B_at_nodes[ElemNodes]
    Bj           = Bjk.mean(axis=0)
    if len(GC)>0:
        Bj[GC[:,0]]  = Bj[GC[:,1]]

    # Value of Water Level Surface at the finer mesh points
    W_at_nodes    = my_interpolate(NodeCoords, raw_test_values["initial_values"]["W"].T)
    Wjk           = W_at_nodes[ElemNodes]
    Wj            = Wjk.mean(axis=0)
    Wj            = np.maximum(Wj, Bj)
    if len(GC)>0:
        Wj[GC[:,0]]  = Wj[GC[:,1]]

    # Value of Water Depth at the finer mesh points
    Hj            = Wj - Bj
    if len(GC)>0:
        Hj[GC[:,0]]  = Hj[GC[:,1]]

    # Generates Initial Condition for Discharge in X and Y:
    U0_at_nodes   = my_interpolate(NodeCoords, raw_test_values["initial_values"]["U0"].T)
    U0jk          = U0_at_nodes[ElemNodes]
    U0j           = U0jk.mean(axis=0)
    HU0j           = U0j * Hj
    if len(GC)>0:
        HU0j[GC[:,0]]  = HU0j[GC[:,1]] * GC[:,2]

    V0_at_nodes   = my_interpolate(NodeCoords, raw_test_values["initial_values"]["V0"].T)
    V0jk 	  = V0_at_nodes[ElemNodes]
    V0j  	  = V0jk.mean(axis=0)
    HV0j  	  = V0j * Hj
    if len(GC)>0:
        HV0j[GC[:,0]]  = HV0j[GC[:,1]] * GC[:,2]

    # Get the element cells for the Time Series and make sure
    #     everything will exist when writing the file
    TS_compute = raw_test_values.has_key("time_series") and \
                 raw_test_values["time_series"].has_key("compute") and \
                 raw_test_values["time_series"]["compute"] and \
                 raw_test_values["time_series"].has_key("points") and \
                 raw_test_values["time_series"].has_key("dt_save")

    if TS_compute:
        TS = raw_test_values["time_series"]
        TS_points =  np.array(TS["points"])
        TS_cells = tri.find_simplex(TS_points)
        # Drop the SP cells outside the domain
        ind = (TS_cells!=-1)
        if len(TS_cells)>0 and ind.any():
            TS["cells"] = TS_cells[ind]
            TS["points"] = TS_points[ind,:]
        if sum(ind)<len(ind):
            print "\tDropping (some) Time Series points outside the domain"
    else:
        raw_test_values["time_series"]["compute"] = False
        raw_test_values["time_series"]["cells"] = []
        raw_test_values["time_series"]["points"] = []
        raw_test_values["time_series"]["dt_save"] = 1.0

    # Get the element cells for the Spatial Profile and make sure
    #     everything will exist when writing the file
    SP_compute = raw_test_values.has_key("spatial_profile") and \
                 raw_test_values["spatial_profile"].has_key("compute") and \
                 raw_test_values["spatial_profile"]["compute"] and \
                 raw_test_values["spatial_profile"].has_key("points") and \
                 raw_test_values["spatial_profile"].has_key("sampling_times")

    if SP_compute:
        SP = raw_test_values["spatial_profile"]
        SP_points =  np.array(SP["points"])
        SP_cells = tri.find_simplex(SP_points)
        # Drop the SP cells outside the domain
        ind = (SP_cells!=-1)
        if len(SP_cells)>0 and ind.any():
            SP["cells"] = SP_cells[ind]
            SP["points"] = SP_points[ind,:]
        if sum(ind)<len(ind):
            print "\tDropping (some) Spatial Profile points outside the domain"
    else:
        raw_test_values["spatial_profile"]["compute"] = False
        raw_test_values["spatial_profile"]["cells"] = []
        raw_test_values["spatial_profile"]["points"] = []
        raw_test_values["spatial_profile"]["sampling_times"] = []

    # The Meter2Degrees keyword should be always provided. Empty if values not provided.
    Meter2Degrees = raw_test_values["Meter2Degrees"] if raw_test_values.has_key("Meter2Degrees") else None

    # Pack again
    updated_initial_values = {"NodeCoords"    : NodeCoords.T,
                          "ElemNodes"     : ElemNodes.T,
                          "ElemNeigh"     : ElemNeigh.T,
                          "ElemNeighSides": ElemNeighSides.T,
                          "ElemSides"     : ElemSides.T,
                          "GhostCells"    : GC,
                          "Bj"            : Bj,
                          "Wj"            : Wj,
                          "HU0j"          : HU0j,
                          "HV0j"          : HV0j }
    if saveBnodes:
	updated_initial_values["Bjk"] = Bjk.T
    if "linearization_point" in raw_test_values:
        LinearizationPoint = raw_test_values["linearization_point"]
    else:
        LinearizationPoint = (0,0) # Default value
    return {"initial_values"      : updated_initial_values,
            "linearization_point" : LinearizationPoint,
            "execution_parameters": raw_test_values["execution_parameters"],
            "references"          : raw_test_values["references"],
            "movie"               : raw_test_values["movie"],
            "inundation"          : raw_test_values["inundation"],
            "heatmap"             : raw_test_values["heatmap"],
            "arrival_time"        : raw_test_values["arrival_time"],
            "time_series"         : raw_test_values["time_series"],
            "spatial_profile"     : raw_test_values["spatial_profile"]}
