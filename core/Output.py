import os
import numpy as np

import Default

if Default.DEBUG_MODE:
    from IPython import embed

################################################################################
# Save the execution parameters
################################################################################
def save_execution_parameters(path, filename, test_values):
  file_path = os.path.join(path, filename)
  # Write the file
  with open(file_path,"w") as fh:
      fh.write("#=======================================================================================\n")
      fh.write("# SETUP FOR EXECUTION \n")
      fh.write("#=======================================================================================\n")
      fh.write("nElems	    : %d\n" %test_values["initial_values"]["ElemNodes"].shape[0])
      fh.write("nNodes      : %d\n" %test_values["initial_values"]["NodeCoords"].shape[0])
      fh.write("nGhostCells : %d\n" %test_values["initial_values"]["GhostCells"].shape[0])
      fh.write("#=======================================================================================\n")
      fh.write("Tmax    : %.5f\n" %test_values["execution_parameters"]["Tmax"])
      fh.write("Tol     : %.5E\n" %test_values["execution_parameters"]["tol_dry"])
      fh.write("g       : %.5f\n" %test_values["execution_parameters"]["g"])
      fh.write("manning	: %.5f\n" %test_values["execution_parameters"]["manning"])
      fh.write("#=======================================================================================\n")
      fh.write("SaveMovie : %d\n" %int(test_values["movie"]["compute"]))
      if test_values["movie"]["compute"]:
          fh.write("dtMovie   : %.5f\n" %test_values["movie"]["dt_save"])
      else:
          fh.write("dtMovie   : %.5f\n" %test_values["execution_parameters"]["Tmax"])
      fh.write("#=======================================================================================\n")
      fh.write("SaveTimeSeries : %d\n" %int(test_values["time_series"]["compute"]))
      if test_values["time_series"]["compute"]:
          fh.write("npTimeSeries   :  %d\n" %len(test_values["time_series"]["points"]))
          fh.write("dtTimeSeries   : %.5f\n" %float(test_values["time_series"]["dt_save"]))
      else:
          fh.write("npTimeSeries   : %d\n" %int(0))
          fh.write("dt_save        : %.5f\n" %0.0)
      fh.write("#=======================================================================================\n")
      fh.write("SaveSpatialProfiles : %d\n" %int(test_values["spatial_profile"]["compute"]))
      fh.write("npSpatialProfiles   : %d\n" %len(test_values["spatial_profile"]["points"]))
      fh.write("ntSpatialProfiles   : %d\n" %len(test_values["spatial_profile"]["sampling_times"]))
      fh.write("#=======================================================================================\n")
      fh.write("SaveArrivalTimes : %d\n" %int(test_values["arrival_time"]["compute"]))
      if test_values["arrival_time"]["compute"]:
          fh.write("HeightThreshold  : %.6E\n" %test_values["arrival_time"]["height_threshold"])
      else:
          fh.write("HeightThreshold  : %.6E\n" %0.0)
      fh.write("#=======================================================================================\n")
      SaveMaxWaterHeights = test_values["heatmap"]["compute"] or test_values["inundation"]["compute"]
      fh.write("SaveMaxWaterHeights : %d\n" %int(SaveMaxWaterHeights))
      fh.write("#=======================================================================================\n")
  return

################################################################################
# Save the Time Series Configuration File
################################################################################
def save_time_series_configuration(path, filename, TS):
    file_path = os.path.join(path, filename)
    # Load the data  to be saved (dt_save is already saved in the ExecutionParameters.txt)
    points = TS["points"]
    cells = TS["cells"]
    # Create the file
    np_TS, mp_TS = (0,0) if len(cells)==0 else (3, len(cells))
    header = "# %d %d\n" %(np_TS, mp_TS)
    with open(file_path, "w") as fh:
        fh.write(header)
        for ((x_i, y_i), cell_i) in zip(points, cells):
          fh.write("%+.8E %+.8E %d\n"%(x_i, y_i, cell_i))
    return

################################################################################
# Save the Time Series Reference Values File
################################################################################
def save_time_series_values(path, filename, TS):
    file_path = os.path.join(path, filename)
    # Check if nothing to write
    if not TS.has_key("measured_values") or len(TS["measured_values"])==0:
        header = "# 0 0\n"
        with open(file_path, "w") as fh:
            fh.write(header)
        return
    # This occurrs only if TS has the key measured values
    values = TS["measured_values"]
    nt_TS = len(values[0][0]) # list items are in format (t_i, Wj_i)
    np_TS = len(values)
    # Save the file
    header = "# %d %d\n" %(np_TS, nt_TS)
    with open(file_path, "w") as fh:
        fh.write(header)
        for t_array, Wj_array in values:
            line = " ".join(["%+.8E" %t_i for t_i in t_array])
            fh.write(line + "\n")
            line = " ".join(["%+.8E" %Wj_i for Wj_i in Wj_array])
            fh.write(line + "\n")
    return

    #N_TS = len(TS_values)/2
    #Nt = 0 if len(TS_values)==0 else len(TS_values[0])
    # Load the file
    file_path = os.path.join(path, filename)
    header = "# %d %d\n" %(N_TS, Nt)
    with open(file_path, "w") as fh:
        fh.write(header)
        for i in range(N_TS):
             t = TS_values[2*i]
             vals = TS_values[2*i+1]
             line = " ".join(["%+.8E" %t for t in t])
             fh.write(line + "\n")
             line = " ".join(["%+.8E" %t for t in vals])
             fh.write(line + "\n")

    return

################################################################################
# Save the Spatial Profile Configuration File
################################################################################
def save_spatial_profiles_configuration(path, filename, SP):
    file_path = os.path.join(path, filename)
    # Unpack
    t = SP["sampling_times"]
    points = SP["points"]
    cells = SP["cells"]
    # Create the file
    nt_SP, mt_SP = (0,0) if len(t)==0 else (1, len(t))
    header1 = "# %d %d\n" %(nt_SP, mt_SP)
    np_SP, mp_SP = (0,0) if len(cells)==0 else (len(cells),3)
    header2 = "# %d %d\n" %(np_SP, mp_SP)
    with open(file_path, "w") as fh:
        fh.write(header1)
        line = " ".join(["%+.8E" %t_i for t_i in t])
        fh.write(line + "\n")
        fh.write(header2)
        for ((x_i, y_i), cell_i) in zip(points, cells):
          fh.write("%+.8E %+.8E %d\n"%(x_i, y_i, cell_i))
    return

################################################################################
# Save the Spatial Profile Reference Values File
################################################################################
def save_spatial_profiles_values(path, filename, SP):
    file_path = os.path.join(path, filename)
    # Check if nothing to write
    if not SP.has_key("measured_values"):
        header = "# 0 0\n"
        with open(file_path, "w") as fh:
            fh.write(header)
        return
    # This occurrs only if SP has the key measured values
    values = SP["measured_values"]
    t = values["sampling_times"] if values.has_key("sampling_times") else []
    Wj = values["Wj"] if values.has_key("Wj") else []
    # Save the file
    header = "# %d %d\n" %(len(t), len(Wj))
    with open(file_path, "w") as fh:
        fh.write(header)
        for t_i, Wj_i in zip(t, Wj):
            fh.write("%+.8E\n" %t_i)
            line = " ".join(["%+.8E" %t for t in Wj_i])
            fh.write(line + "\n")
    return
