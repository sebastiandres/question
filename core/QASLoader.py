import numpy as np
import pickle
import sys
import os

import MeshUtilities as mu
import QASFormat as QAS
import LonLatHandler
import Default
import Common

if Default.DEBUG_MODE:
    from IPython import embed

'''
WARNING: Do not use this library directly. Use SBFFormat instead.
'''

################################################################################
# LOADS CONDITIONALY USING from_folder
################################################################################
def load_mesh_dict(from_folder):
    """
    The task is to conditionally read the from_folder, which can have two formats:
    question format or a simplified format, explain in the documentation.
    """
    fpath = os.path.abspath(from_folder)

    if QAS.has_valid_format(fpath):
        print("Mesh refiner can be continued from last iteration.")
        return load_pickled_qas_mesh(fpath)

    # If not in QAS
    print("--from folder {0} does not have QAS format.".format(from_folder))
    return sys.exit(-1)

################################################################################
# LOADS MESH ASSUMING QUESTION STRUCTURE
################################################################################
def load_qas_formatted_mesh(fpath):
    """
    Loads the data in question format and creates a dictionary with the values.
    """
    ################################################################################
    # Suggested parameter usage
    ################################################################################
    # I need to read this from file
    test_BC = ["soft", "soft", "soft", "soft"]
    references = []
    exec_params = {"Tmax":4*60*60,
                   "g":Default.g(),
                   "manning":Default.manning(),
                   "tol_dry":Default.tol_dry()}
    ##################
    # MANDATORY FILES
    ##################
    # Mesh
    NodeCoords = np.loadtxt(os.path.join(fpath, "Mesh", "NodeCoords.txt"), dtype=float)
    ElemNodes = np.loadtxt(os.path.join(fpath, "Mesh", "ElemNodes.txt"), dtype=int)
    cell_x, cell_y = Common.get_centroids(NodeCoords, ElemNodes)
    # Bathymetry: (B)
    B_aux = np.loadtxt(os.path.join(fpath, "InitialConditions", "Bathymetry.txt"), dtype=float)
    B_at_cells = np.array([cell_x, cell_y, B_aux])
    B_aux = mu.my_interpolate(NodeCoords, B_at_cells)
    B_at_nodes = np.array([NodeCoords[:,0], NodeCoords[:,1], B_aux]).T
    # Water Level Surface: (W)
    W_aux = np.loadtxt(os.path.join(fpath, "InitialConditions", "WaterLevel.txt"), dtype=float)
    W_at_cells = np.array([cell_x, cell_y, W_aux])
    W_aux = mu.my_interpolate(NodeCoords, W_at_cells)
    W_at_nodes = np.array([NodeCoords[:,0], NodeCoords[:,1], W_aux]).T
    # Water Depth (h)
    H = W_at_nodes[:,2] - B_at_nodes[:,2]
    dry_cells = (H<=0.0)
    H[dry_cells] = np.nan
    # Discharge X
    HU0_aux = np.loadtxt(os.path.join(fpath, "InitialConditions", "DischargeX.txt"), dtype=float)
    HU0_at_cells = np.array([cell_x, cell_y, HU0_aux])
    HU0_aux = mu.my_interpolate(NodeCoords, HU0_at_cells)
    HU0_at_nodes = np.array([NodeCoords[:,0], NodeCoords[:,1], HU0_aux]).T
    U0_at_nodes = HU0_at_nodes.copy()
    U0_at_nodes[:,2] = HU0_at_nodes[:,2] / H
    U0_at_nodes[dry_cells,2] = 0.0
    # Discharge Y
    V0_at_nodes = U0_at_nodes
    # Pack
    unrefined_initial_values = {"Mesh":NodeCoords,
                                "B":B_at_nodes,
                                "W":W_at_nodes,
                                "U0":U0_at_nodes,
                                "V0":V0_at_nodes,
                                "BC_list":test_BC}

    ################################################################################
    # Required Output
    ################################################################################
    # By default, all activated. User can desactivate directly overwritting the ExecutionParameters.txt
    # Movie Animation Step
    movie = {"compute":True, "dt_save":300}

    # inundation map, heat map and arrival times maps
    inundation   = {"compute":True}
    heatmap      = {"compute":True}
    arrival_time = {"compute":True,
                    "height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Configuration
    # Buoys
    #TimeSeries = np.loadtxt(os.path.join(fpath, "Output", "TimeSeriesConfig.txt"), dtype=float)
    #TimeSeries = TimeSeries[:,:2]
    BuoyCoords = []

    TS   = {"compute":False}
    TS["points"]  = BuoyCoords
    TS["dt_save"] = 60.

    # Time Series Known Values
    """
    TS_values = []
    TS_files  = os.path.join(fpath, "TimeSeries_Values.txt")
    for j in range(len(x_TS)):
        t_TS, w_TS = time_serie_values(TS_files,j,len(x_TS))
        TS_values.append(t_TS)
        TS_values.append(w_TS)
    TS["measured_values"] = TS_values
    """
    # Spatial Profile Values
    SP   = {"compute":False}

    # LonLat
    m2deg_fpath = os.path.join(fpath, "Mesh", "Optional", "Meter2Degrees.txt")
    if os.path.exists(m2deg_fpath):
        m2deg =  np.loadtxt(m2deg_fpath, dtype=float)
    else:
        m2deg = None
    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"unrefined_initial_values": unrefined_initial_values,
            "execution_parameters": exec_params,
            "references"          : references,
            "Meter2Degrees"       : m2deg,
            "movie"               : movie,
            "inundation"          : inundation,
            "heatmap"             : heatmap,
            "arrival_time"        : arrival_time,
            "time_series"         : TS,
            "spatial_profile"     : SP}
    return sys.exit(-1)


'''
def load_pickled_qas_mesh(fpath):
    """
    Loads the data in question format and creates a dictionary with the values.
    """
    pickle_filepath = os.path.join(fpath, "Mesh", "Optional", "original_unrefined_variables.pkl")
    node_coords_filepath = os.path.join(fpath, "Mesh", "NodeCoords.txt")
    ghost_cells_filepath = os.path.join(fpath, "Mesh", "GhostCells.txt")
    m2d_filepath = os.path.join(fpath, "Mesh", "Optional", "Meter2Degrees.txt")
    # Load the pickle file
    pickle_reader = open(pickle_filepath, "r")
    unrefined_mesh_dict = pickle.load(pickle_reader)
    # Load the nodes and ghostcells to recreate the last nodes
    NodeCoords = np.loadtxt(node_coords_filepath, dtype=float)
    GhostCells = np.loadtxt(ghost_cells_filepath, dtype=float)
    # Convert them from LonLat to degrees, if need be
    if os.path.exists(m2d_filepath):
        Meter2Degrees = np.loadtxt(m2d_filepath)
        LonLat = LonLatHandler.convert_array_to_lonlat(NodeCoords, Meter2Degrees)
        NodeCoords = LonLat
    # Only keep nodes that weren't added as Ghost Cells
    # Actually, each Ghost Cell correspond to a unique new node
    # We will take advantage of this fact!
    n_not_GC = NodeCoords.shape[0] - GhostCells.shape[0]
    unrefined_mesh_dict["NodeCoords"] = NodeCoords[:n_not_GC]
    return unrefined_mesh_dict
'''

'''
################################################################################
# LOADS MESH ASSUMING STANDARD STRUCTURE
################################################################################
def load_simplified_formatted_mesh(fpath):
    """
    Loads the data in simplified format and creates a dictionary with the values.
    """
    # References
    references = []

    ################################################################################
    # Suggested parameter usage
    ################################################################################
    test_default_BCs = ["soft", "soft", "soft", "soft"]
    test_BC = test_default_BCs #Common.assign_BC(terminal_BCs, test_default_BCs)
    exec_params = {"Tmax":Default.tsunami_simulation_Tmax(nhours=6),
                   "g":Default.g(),
                   "manning":Default.manning(),
                   "tol_dry":Default.tol_dry()}

    # Load with specific loader
    mesh, B_at_mesh, W_at_mesh, BuoyCoords, BuoyValues = SBF.load(fpath)
    x, y = mesh[:,0], mesh[:,1]
    # Mesh
    B = np.array([x, y, B_at_mesh]).T
    # Water Level
    W = np.array([x, y, W_at_mesh]).T
    # Discharges
    U0 = np.array([x, y, np.zeros(len(x))]).T
    V0 = np.array([x, y, np.zeros(len(x))]).T

    # Pack
    unrefined_initial_values = {"Mesh":mesh, "B":B, "W":W, "U0":U0, "V0":V0, "BC_list":test_BC}

    ################################################################################
    # Required Output
    ################################################################################
    # By default, all activated. User can desactivate directly overwritting the ExecutionParameters.txt
    # Movie Animation Step
    movie = {"compute":True, "dt_save":300} # save only each 5 minutes

    # inundation map, heat map and arrival times maps
    inundation   = {"compute":True}
    heatmap      = {"compute":True}
    arrival_time = {"compute":True,
                    "height_threshold":Default.arrival_time_height_threshold()}

    # Time Series Configuration
    TS_compute = ( len(BuoyCoords)!=0 )
    if TS_compute:
        TS   = {"compute":True}
        TS["points"]  = BuoyCoords
        TS["dt_save"] = 60.
    else:
        TS   = {"compute":False}

    # Time Series Known Values
    # Fix measured values
    print("!!!*** Fix simplified formad MeshLoader loading of Time Series Values")
    TS["measured_values"] = []

    # Spatial Profile Values
    SP   = {"compute":False}

    # LonLat
    print("Generating the Meter 2 Degrees Table")
    Lon_min = mesh[:,0].min()
    Lon_max = mesh[:,0].max()
    Lat_min = mesh[:,1].min()
    Lat_max = mesh[:,1].max()
    LonLat_grid_dic = {"dL":None, "dT":None, "NElems":10000, "triangles":"rectangular"}

    # Generate but don't print a grid for the LonLat
    Lon, Lat   = Common.get_grid(Lon_min, Lon_max, Lat_min, Lat_max, LonLat_grid_dic,
                                 filename="", verbose=False)
    Lon0, Lat0 = .5*(Lon_min+Lon_max), .5*(Lat_min+Lat_max)
    m2deg = LonLatHandler.generate_meter2lonlat_table(Lon, Lat, Lon0, Lat0)


    ################################################################################
    # Return dic with raw test values
    ################################################################################
    return {"unrefined_initial_values": unrefined_initial_values,
            "execution_parameters": exec_params,
            "references"          : references,
            "Meter2Degrees"       : m2deg,
            "movie"               : movie,
            "inundation"          : inundation,
            "heatmap"             : heatmap,
            "arrival_time"        : arrival_time,
            "time_series"         : TS,
            "spatial_profile"     : SP}
'''
