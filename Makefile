# QUESTION'S MAKEFILE
# .PHONY. allows to always execute the instruction, (no checking for the existence)

ALL_TESTS = -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 20 21 22 23 24 25 40 41 42 43 44 45 46
SOME_TESTS = -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 20 21 22 23 24 25
FEW_TESTS =  1 2 20 21

.PHONY.: options
options:
	@grep [a-z]: Makefile

.PHONY.: pull
pull:
	git pull origin master

.PHONY.: push
push:
	git push -u origin master

.PHONY.: tests
tests:
	@mkdir -p tmp
	@for var in $(ALL_TESTS); do \
		echo TEST $$var ; \
		question.py tmp/test$$var --test $$var > tmp/test$$var.question ; \
		grep "invalid\|success" tmp/test$$var.question ; \
	done
	@echo "\nTEST --user_defined_flags and --flat"
	@question.py tmp/test4_flat --user_defined_files tmp/test4 --flat 2 > tmp/test4_flat.question
	@grep "invalid\|success" tmp/test4_flat.question
	@echo "\nTEST --user_defined_flags and --bell"
	@question.py tmp/test4_bell --user_defined_files tmp/test4 --bell 0 0 5 1000 > tmp/test4_bell.question
	@grep "invalid\|success" tmp/test4_bell.question
	@echo "\nTEST --user_defined_flags and --wave"
	@question.py tmp/test4_wave --user_defined_files tmp/test4 --wave 0 0 5 1000 45 > tmp/test4_wave.question
	@grep "invalid\|success" tmp/test4_wave.question
	@echo "\nAll required tests have been performed\n"

.PHONY.: profile
profile:
	@mkdir -p tmp
	@for var in $(ALL_TESTS); do \
		echo \\nTEST $$var ; \
		python -m cProfile -o tmp/test$$var.question_profile question.py tmp/test$$var --test $$var > tmp/test$$var.question ; \
		grep "invalid\|success" tmp/test$$var.question ; \
	done
	@echo "\nTEST --user_defined_flags and --flat"
	@python -m cProfile -o tmp/test4_flat.question_profile question.py tmp/test4_flat --user_defined_files tmp/test4 --flat 2 > tmp/test4_flat.question
	@grep "invalid\|success" tmp/test4_flat.question
	@echo "\nTEST --user_defined_flags and --bell"
	@python -m cProfile -o tmp/test4_bell.question_profile question.py tmp/test4_bell --user_defined_files tmp/test4 --bell 0 0 5 1000 > tmp/test4_bell.question
	@grep "invalid\|success" tmp/test4_bell.question
	@echo "\nTEST --user_defined_flags and --wave"
	@python -m cProfile -o tmp/test4_wave.question_profile question.py tmp/test4_wave --user_defined_files tmp/test4 --wave 0 0 5 1000 45 > tmp/test4_wave.question
	@grep "invalid\|success" tmp/test4_wave.question
	@echo "\nAll required tests have been performed\n"

.PHONY.: clean
clean:
	@find . -name "*.pyc" -delete
	@find . -name "*.py~" -delete
	@find . -name "*.pyo" -delete
	@rm -rf core/field_test/tmp
	@rm -rf tmp
