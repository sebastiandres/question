from IPython import embed

# This import the little trick in the boiler plate
import os
os.sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from core.Okada import LocalNotation, AuxiliarParameters,  okada_raw_model
from core.Okada import atan_stable
from core.Okada import TOL_DELTA_0, TOL_DELTA_1


import numpy as np
from numpy import cos, sin, tan, sqrt

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

# Fix some matplotlib notation
from matplotlib import ticker
x_formatter = ticker.ScalarFormatter(useOffset=False)
y_formatter = ticker.ScalarFormatter(useOffset=False)

# Constants
DIRECTIONS = ["strike_slip", "dip_slip","tensile_fault"]
PATH = "tmp"

################################################################
# Test the definition of atan_stable
################################################################
def test_limits(A, B, C):
    def limit_function(delta):
        #num = atan_stable(A + B*sin(delta) + C*cos(delta), 1)
        num = atan_stable((1-sin(delta)**2), cos(delta))
        den = cos(delta)
        return num/den
    # Plot a case
    N = 3601
    theta = np.linspace(-np.pi/2,np.pi/2, N)
    theta_deg = theta*180./np.pi
    lim = limit_function(theta)
    #embed()
    plt.plot(theta_deg, lim, 'o')
    plt.suptitle("A={0}, B={1}, C={2}".format(A,B,C))
    plt.show()
    return

################################################################
# Test the definition of atan_stable
################################################################
def plot_atan():
    # Print some important cases
    embed()
    #atan_stable(num, den)
    inf = np.inf
    hpi = np.pi/2.
    qpi = np.pi/4.
    den = [ +1., +1., -1., -1.] # x
    num = [ +1., -1., +1., -1.] # y
    val = [+qpi,-qpi,-qpi,+qpi] # value
    for n,d,v in zip(num, den, val):
        rv = atan_stable(n,d)
        print "atan(%+f,%+f) should be %+f and returned %+f (delta %f)" %(n,d,v,rv, v-rv)
    print ""
    den = [+1.,+1.,-1.,-1.,+inf,-inf] # x
    num = [+0.,-0.,+0.,-0., +0., +0.] # y
    val = [+0.,-0.,-0.,+0., +0., -0.] # value
    for n,d,v in zip(num, den, val):
        rv = atan_stable(n,d)
        print "atan(%+f,%+f) should be %+f and returned %+f (delta %f)" %(n,d,v,rv, v-rv)
    print ""
    den = [ +0., -0., +0., -0., +0., +0.] # x
    num = [ +1., +1., -1., -1.,+inf,-inf] # y
    val = [+hpi,+hpi,-hpi,-hpi,+hpi,-hpi] # value
    for n,d,v in zip(num, den, val):
        rv = atan_stable(n,d)
        print "atan(%+f,%+f) should be %+f and returned %+f (delta %f)" %(n,d,v,rv, v-rv)

    # Plot a case
    N = 3601
    theta = np.linspace(-np.pi,np.pi, N)
    theta_deg = theta*180./np.pi
    R = np.linspace(2., 2., N)
    x = R*cos(theta)
    y = R*sin(theta)
    theta_inv = atan_stable(y, x)
    plt.figure(figsize=(14,12))
    plt.subplot(311)
    plt.plot(x,y)
    plt.subplot(312)
    plt.plot(theta_deg, theta, label="orig")
    plt.plot(theta_deg, theta_inv, label="num")
    plt.legend()
    plt.subplot(313)
    plt.plot(theta_deg, theta-theta_inv)
    plt.show()


    return

################################################################
# Test the definition of Auxiliar Coefficients.
# We should get continuity at 0
################################################################
def get_I(x, y, L, W, d, delta, Lambda, Mu):
    # Variables
    pihalf = 0.5*np.pi
    x_i = L/2 #x #L/2 #x.mean()
    y_i = W/2 #y #W/2 #y.mean()
    delta_list = []
    # Create some values to be studied
    N0 = 360
    #delta_array0 = np.linspace(-np.pi, np.pi, 2*N0-1)
    delta_array0 = np.linspace(-90., 90., 2*N0-1)*np.pi/180.
    delta_list.append(delta_array0)
    N1 = 40
    delta_array1_l = np.linspace(-pihalf-1E-3,-pihalf+1E-3, 2*N1-1)
    delta_array1_r = np.linspace(+pihalf-1E-3,+pihalf+1E-3, 2*N1-1)
    delta_list.append(delta_array1_l)
    delta_list.append(delta_array1_r)
    N2 = 40
    delta_array2_l = np.linspace(-pihalf-1E-5,-pihalf+1E-5, 2*N2-1)
    delta_array2_r = np.linspace(+pihalf-1E-5,+pihalf+1E-5, 2*N2-1)
    delta_list.append(delta_array2_l)
    delta_list.append(delta_array2_r)
    N3 = 40
    delta_array3_l = np.linspace(-pihalf-1E-7,-pihalf+1E-7, 2*N3-1)
    delta_array3_r = np.linspace(+pihalf-1E-7,+pihalf+1E-7, 2*N3-1)
    delta_list.append(delta_array3_l)
    delta_list.append(delta_array3_r)
    N4 = 40
    delta_array4_l = np.linspace(-pihalf-1E-9,-pihalf+1E-9, 2*N4-1)
    delta_array4_r = np.linspace(+pihalf-1E-9,+pihalf+1E-9, 2*N4-1)
    delta_list.append(delta_array4_l)
    delta_list.append(delta_array4_r)
    """
    N5 = 40
    delta_array5_l = np.linspace(-pihalf-1E-11,-pihalf+1E-11, 2*N5-1)
    delta_array5_r = np.linspace(+pihalf-1E-11,+pihalf+1E-11, 2*N5-1)
    delta_list.append(delta_array5_l)
    delta_list.append(delta_array5_r)
    N6 = 40
    delta_array6_l = np.linspace(-pihalf-1E-13,-pihalf+1E-13, 2*N6-1)
    delta_array6_r = np.linspace(+pihalf-1E-13,+pihalf+1E-13, 2*N6-1)
    delta_list.append(delta_array6_l)
    delta_list.append(delta_array6_r)
    """
    # Get the values
    I1_list, I2_list, I3_list, I4_list, I5_list = [], [], [], [], []
    n = len(delta_list)
    for i in range(n):
        delta_array = delta_list[i]
        I = list()
        for delta_i in delta_array:
            xi = x_i
            eta = y_i*cos(delta_i) + d*sin(delta_i)
            LocalNotation(xi, eta, x_i, y_i, d, delta_i)
            I.append(AuxiliarParameters(xi, eta, x_i, y_i, d, delta_i, Mu, Lambda))
        # Convert to array
        I = np.array(I)
        I1, I2, I3, I4, I5 = I[:,0], I[:,1], I[:,2], I[:,3], I[:,4]
        # Append
        I1_list.append(I1)
        I2_list.append(I2)
        I3_list.append(I3)
        I4_list.append(I4)
        I5_list.append(I5)

    # Return the values
    return delta_list, I1_list, I2_list, I3_list, I4_list, I5_list

def plot_I(delta_list, I_list, ylabel="", suptitle=""):
    """
    deltas_list, I_list: list with values, douh!
    """
    pihalf = 0.5*np.pi
    fig = plt.figure(figsize=(14,12))
    n = (len(delta_list)-1)/2 + 1
    # Top plot
    ax = plt.subplot2grid((n,2), (0,0), colspan=2)
    I = I_list[0]
    delta_rad = delta_list[0]
    delta_deg = delta_rad*180./np.pi
    ax.plot(delta_deg, I, "bo-")
    for i in range(1,n):
        for j in range(2):
            # Unpack
            ij = 1 + (i-1)*2 + j
            delta_rad = delta_list[ij]
            I = I_list[ij]
            delta_deg = delta_rad*180/np.pi
            # Some index for better plotting
            ind0_r = np.abs(delta_rad - pihalf)<TOL_DELTA_0
            ind0_l = np.abs(delta_rad + pihalf)<TOL_DELTA_0
            ind1_r = np.abs(delta_rad - pihalf)<TOL_DELTA_1
            ind1_l = np.abs(delta_rad + pihalf)<TOL_DELTA_1
            ax = plt.subplot2grid((n,2), (i,j))
            ax.plot(delta_deg, I, "bo-")
            ax.plot(delta_deg[ind1_r], I[ind1_r], "go-")
            ax.plot(delta_deg[ind0_r], I[ind0_r], "rs-")
            ax.plot(delta_deg[ind1_l], I[ind1_l], "go-")
            ax.plot(delta_deg[ind0_l], I[ind0_l], "rs-")
            ax.tick_params(axis='both', which='major', labelsize=6)
            ax.tick_params(axis='both', which='minor', labelsize=6)
            ax.xaxis.set_major_formatter(x_formatter)
            ax.yaxis.set_major_formatter(y_formatter)
            if i==(n-1):
                ax.set_xlabel("$\delta$ [deg]")
            ax.set_ylabel(ylabel)
    #plt.subplots_adjust(wspace=0.3, hspace=0.3)
    plt.suptitle(suptitle, fontsize=18)
    plt.show()
    return

################################################################
# Plot the views
################################################################
def plot_displacements(x, y, L, W, d, delta, Lambda, Mu, plot, show=False):
    """
    """
    basename = os.path.join(PATH, plot)

    # Some parameters
    if plot==DIRECTIONS[0]: # Strike Slip
        U1, U2, U3 = 30., 0., 0.
        xticks = np.linspace(-500, 400, 10)
        yticks = np.linspace(-400, 400, 9)
        zticks = np.linspace(-0.3, 0.4, 8)
        elev, azim = 17, -121
    elif plot==DIRECTIONS[1]: # Dip slip
        U1, U2, U3 = 0., 30., 0.
        xticks = np.linspace(-400, 400, 9)
        yticks = np.linspace(-400, 400, 9)
        zticks = np.linspace(-0.2, 0.5, 8)
        elev, azim = 9, -155
    elif plot==DIRECTIONS[2]: # Tensile fault
        U1, U2, U3 = 0., 0., 30.
        xticks = np.linspace(-500, 400, 10)
        yticks = np.linspace(-600, 400, 11)
        zticks = np.linspace(-0.2, 1.2, 8)
        elev, azim = 31, -125
    else: # General case
        U1, U2, U3 = 10., 10., 10.
        xticks = np.linspace(min(x1), max(x1), 9)
        yticks = np.linspace(min(x2), max(x2), 9)
        zticks = np.linspace(-0.2, 1.2, 8)
        elev, azim = 31, -125

    # Apply Okada Model
    u = okada_raw_model(x, y, U1, U2, U3, L, W, d, delta, Mu, Lambda)
    ################################################################
    # Plot
    ################################################################

    # Displacement magnitude
    U = sqrt(U1**2+U2**2+U3**2)

    # Plot the Okada surface
    x1 = (x+u[:,0])/1E3 # in km
    x2 = (y+u[:,1])/1E3 # in km
    x3 = (0+u[:,2])/1E3 # in km
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_trisurf(x1, x2, x3, cmap=cm.jet, linewidth=0.0, alpha=0.5)
    vmin = np.min([x1.min(), x2.min(), x3.min()])
    vmax = np.max([x1.max(), x2.max(), x3.max()])
    ax.set_xlim3d(vmin,vmax)
    ax.set_ylim3d(vmin,vmax)
    ax.set_zlim3d(vmin,vmax)
    # Plot the Fault
    x_fault = np.array([0, L, L, 0])
    y_fault = np.array([0, 0, W*cos(delta), W*cos(delta)])
    z_fault = np.array([-d, -d, -d+W*sin(delta), -d+W*sin(delta)])
    ax.plot_trisurf(x_fault/1E3,y_fault/1E3,z_fault/1E3)
    ax.set_xlabel('x [km]')
    ax.set_ylabel('y [km]')
    plt.savefig(basename+"_fault_okada.png")
    if show:
        plt.show()
    else:
        plt.close()

    # Plot the x-component of the okada surface
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    X = x.reshape((N,N))
    Y = y.reshape((N,N))
    Z = u[:,0].reshape((N,N))
    ax.plot_surface(X/1.E3, Y/1.E3, Z/U, rstride=1, cstride=1, cmap=cm.jet, linewidth=0.2, alpha=0.5)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_xticks(xticks)
    ax.set_xlim3d(min(xticks), max(xticks))
    ax.set_yticks(yticks)
    ax.set_ylim3d(min(yticks), max(yticks))
    ax.set_zticks(zticks)
    ax.set_zlim3d(min(zticks), max(zticks))
    ax.view_init(elev=elev, azim=azim)
    ax.set_title(plot.title().replace("_"," ") + ": (x+ux,y,z)")
    plt.savefig(basename+"_ux_okada.png")
    if show:
        plt.show()
    else:
        plt.close()

    # Plot the y-component of the okada surface
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    X = x.reshape((N,N))
    Y = y.reshape((N,N))
    Z = u[:,1].reshape((N,N))
    ax.plot_surface(X/1.E3, Y/1.E3, Z/U, rstride=1, cstride=1, cmap=cm.jet, linewidth=0.2, alpha=0.5)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_xticks(xticks)
    ax.set_xlim3d(min(xticks), max(xticks))
    ax.set_yticks(yticks)
    ax.set_ylim3d(min(yticks), max(yticks))
    ax.set_zticks(zticks)
    ax.set_zlim3d(min(zticks), max(zticks))
    ax.view_init(elev=elev, azim=azim)
    ax.set_title(plot.title().replace("_"," ") + ": (x,y+uy,z)")
    plt.savefig(basename+"_uy_okada.png")
    if show:
        plt.show()
    else:
        plt.close()

    # Plot the z-component of the okada surface
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    X = x.reshape((N,N))
    Y = y.reshape((N,N))
    Z = u[:,2].reshape((N,N))
    ax.plot_surface(X/1.E3, Y/1.E3, Z/U, rstride=1, cstride=1, cmap=cm.jet, linewidth=0.2, alpha=0.5)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_xticks(xticks)
    ax.set_xlim3d(min(xticks), max(xticks))
    ax.set_yticks(yticks)
    ax.set_ylim3d(min(yticks), max(yticks))
    ax.set_zticks(zticks)
    ax.set_zlim3d(min(zticks), max(zticks))
    ax.view_init(elev=elev, azim=azim)
    ax.set_title(plot.title().replace("_"," ") + ": (x,y,z+uz)")
    plt.savefig(basename+"_uz_okada.png")
    if show:
        plt.show()
    else:
        plt.close()

    # Plot the whole okada surface
    x1 = (x+u[:,0])/1E3 # in km
    x2 = (y+u[:,1])/1E3 # in km
    x3 = (0+u[:,2])/U # in km
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_trisurf(x1,x2,x3, cmap=cm.jet, linewidth=0.1, alpha=0.5)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_xticks(xticks)
    ax.set_xlim3d(min(xticks), max(xticks))
    ax.set_yticks(yticks)
    ax.set_ylim3d(min(yticks), max(yticks))
    ax.set_zticks(zticks)
    ax.set_zlim3d(min(zticks), max(zticks))
    ax.view_init(elev=elev, azim=azim)
    ax.set_title(plot.title().replace("_"," ") + ": (x+ux,y+uy,z+uz)")
    plt.savefig(basename+"_uzyx_okada.png")
    if show:
        plt.show()
    else:
        plt.close()

####################################################################################
####################################################################################
if __name__=="__main__":
    # Parameters
    N = 100

    # Get a grid
    s = np.linspace(-400E3,400E3,N)
    X, Y = np.meshgrid(s,s)
    x = X.flatten()
    y = Y.flatten()
    L = 220.E3
    W = 90.E3
    d = 25.E3
    delta = 13.*np.pi/180.
    nu = 0.25
    E = 1E9
    Lambda = (nu*E)/((1.+nu)*(1.-2.*nu))
    Mu = E/(2.*(1.+nu))

    # make tmp dir
    if not os.path.exists(PATH):
        os.mkdir(PATH)

    # Test your lims
    #test_limits(0., 0., 0.)
    #test_limits(0., 0., 1.)
    #test_limits(1., 1., 1.)

    # Study the arctan
    #plot_atan()


    # Study the Auxiliar I values
    #delta, I1, I2, I3, I4, I5 = get_I(x, y, L, W, d, delta, Lambda, Mu)
    #plot_I(delta, I5, suptitle="$I_5$")
    #plot_I(delta, I1, suptitle="$I_1$")
    #plot_I(delta, I4, suptitle="$I_4$")
    #plot_I(delta, I1, suptitle="$I_1$")

    # Study the displacements
    plot_displacements(x, y, L, W, d, delta, Lambda, Mu, plot=DIRECTIONS[0])
    plot_displacements(x, y, L, W, d, delta, Lambda, Mu, plot=DIRECTIONS[1])
    plot_displacements(x, y, L, W, d, delta, Lambda, Mu, plot=DIRECTIONS[2])
