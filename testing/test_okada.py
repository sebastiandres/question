from ExecutionTesting import system

################################################################################
# Tests that must fail misserably
################################################################################
from_folder = "tmp/inexistent"
to_folder = "tmp/wrong"
system("python question.py --okada {0} --to {1} --refine 0.05 50. 1.0".format(from_folder, to_folder), False)

################################################################################
# Tests that are not executed (because they are handled elegantly)
################################################################################
from_folder = "tmp/inexistent"
to_folder = "tmp/wrong"
system("python question.py --okada {0} --to {1} --refine 0.05 50. 1.0".format(from_folder, to_folder), True)

################################################################################
# Simple Tests that must work
################################################################################
from_folder = "data/okada/okada.txt"
to_folder = "tmp/okada"
system("python question.py --okada {0} --to {1} --refine 0.05 50. 1.0".format(from_folder, to_folder), True)
