from ExecutionTesting import system

################################################################################
# Tests that must fail misserably
################################################################################
"""
for test in [1,20]:
    to_folder = "tmp/wrong"
    system("python question.py --benchmark {0} --to {1} --refine 0.05 50. 1.0".format(test, to_folder), False)

################################################################################
# Tests that are not executed (because they are handled elegantly)
################################################################################

################################################################################
# Simple Tests that must work
################################################################################
for test in tests:
    to_folder = "tmp/benchmark_{0:02d}".format(test)
    system("python question.py --benchmark {0} --to {1}".format(test, to_folder), True)
"""
