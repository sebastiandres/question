from ExecutionTesting import system, fancy_title, get_testing_enviroment
from ExecutionTesting import SHOULD_PASS, SHOULD_FAIL
import os

# Test definitions
debug_set = range(-6,0)
analytical_set = range(1,8)
lab_set = range(20,24)
field_set = [] #range(40,43)
# What to really test
all_tests = debug_set + analytical_set + lab_set + field_set
some_tests = range(1,3)

################################################################################
# Clean before making the tests
################################################################################
env = get_testing_enviroment(clean=True)

################################################################################
# Tests that must fail misserably
################################################################################
fancy_title("Tests that should fail (mixing flags & inexisting flags)")
for test in some_tests:
    to_folder = os.path.join(env, "wrong")
    # flag doesn't exists
    system("python question.py --benchmark {0} --to {1} --not_existing_flag".format(test, to_folder), SHOULD_FAIL)
    # Flag cannot be applied to test
    system("python question.py --benchmark {0} --to {1} --flat 0 0 0".format(test, to_folder), SHOULD_FAIL)

################################################################################
# Tests that should fail
################################################################################
fancy_title("Tests that should fail (mixing flags that shouln't be mixed)")
for test in some_tests:
    to_folder = os.path.join(env, "wrong")
    # Flags dL and NElems cannot be mixed
    system("python question.py --benchmark {0} --dL 1.0 --NElems 5000 --to {1}".format(test, to_folder), SHOULD_FAIL)
    # Flags BC and BC_list cannot be mixed
    system("python question.py --benchmark {0} --BC soft --BC_list wall wall wall wall --to {1}".format(test, to_folder), SHOULD_FAIL)

################################################################################
# Tests that should fail
################################################################################
fancy_title("Tests that should fail (periodic sides must match)")
for test in some_tests:
    to_folder = os.path.join(env, "wrong")
    # Flags BC and BC_list cannot be mixed
    system("python question.py --benchmark {0} --BC_list periodic wall wall wall --to {1}".format(test, to_folder), SHOULD_FAIL)
    # Flags BC and BC_list cannot be mixed
    system("python question.py --benchmark {0} --BC_list periodic wall periodic wall --to {1}".format(test, to_folder), SHOULD_FAIL)

################################################################################
# Simple Tests that must work
################################################################################
fancy_title("Tests that should pass (no flags)")
for test in some_tests:
    to_folder = os.path.join(env, "benchmark_{0:02d}".format(test))
    system("python question.py --benchmark {0} --to {1}".format(test, to_folder), SHOULD_PASS)

fancy_title("Tests that should work (no flags, but folder is already present)")
for test in some_tests:
    to_folder = os.path.join(env, "benchmark_{0:02d}".format(test))
    system("python question.py --benchmark {0} --to {1}".format(test, to_folder), SHOULD_PASS)

################################################################################
# Simple Tests with flags that must work
################################################################################
fancy_title("Tests that should pass (one correct optional flag)")
for test in some_tests:
    # Triangles
    to_folder = os.path.join(env, "benchmark_{0:02d}_triangles_rectangular".format(test))
    system("python question.py --benchmark {0} --triangles rectangular --to {1}".format(test, to_folder), SHOULD_PASS)
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_triangles_equilateral".format(test))
    system("python question.py --benchmark {0} --triangles equilateral --to {1}".format(test, to_folder), SHOULD_PASS)
    # dL
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_dL_1".format(test))
    system("python question.py --benchmark {0} --dL 1.0 --to {1}".format(test, to_folder), SHOULD_PASS)
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_dL_2".format(test))
    system("python question.py --benchmark {0} --dL 2.0 --to {1}".format(test, to_folder), SHOULD_PASS)
    # NELems
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_NElems_8000".format(test))
    system("python question.py --benchmark {0} --NElems 8000 --to {1}".format(test, to_folder), SHOULD_PASS) 
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_NElems_6000".format(test))
    system("python question.py --benchmark {0} --NElems 6000 --triangles rectangular --to {1}".format(test, to_folder), SHOULD_PASS)
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_NElems_4000".format(test))
    system("python question.py --benchmark {0} --NElems 4000 --triangles equilateral --to {1}".format(test, to_folder), SHOULD_PASS)
    # BC
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_BC_wall".format(test))
    system("python question.py --benchmark {0} --BC wall --to {1}".format(test, to_folder), SHOULD_PASS) 
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_BC_soft".format(test))
    system("python question.py --benchmark {0} --BC soft --to {1}".format(test, to_folder), SHOULD_PASS) 
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_BC_periodic".format(test))
    system("python question.py --benchmark {0} --BC periodic --to {1}".format(test, to_folder), SHOULD_PASS) 
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_BC_none".format(test))
    system("python question.py --benchmark {0} --BC none --to {1}".format(test, to_folder), SHOULD_PASS) 
    # BC_list
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_BClist_wwss".format(test))
    system("python question.py --benchmark {0} --BC_list wall wall soft soft --to {1}".format(test, to_folder), SHOULD_PASS)
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_BClist_ssww".format(test))
    system("python question.py --benchmark {0} --BC_list soft soft wall wall --to {1}".format(test, to_folder), SHOULD_PASS)
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_BClist_ppww".format(test))
    system("python question.py --benchmark {0} --BC_list periodic periodic wall wall --to {1}".format(test, to_folder), SHOULD_PASS)
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_BClist_ssp".format(test))
    system("python question.py --benchmark {0} --BC_list soft soft periodic periodic --to {1}".format(test, to_folder), SHOULD_PASS)

################################################################################
# Complex Tests with flags that must work
################################################################################
fancy_title("Tests that should pass (several correct optional flag)")
for test in some_tests:
    # Triangles
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_complex_A".format(test))
    system("python question.py --benchmark {0} --NElems 2500 --triangles rectangular --BC wall --to {1}".format(test, to_folder), SHOULD_PASS)
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}_complex_B".format(test))
    system("python question.py --benchmark {0} --dL 1.0 --triangles equilateral --BC_list wall soft periodic periodic --to {1}".format(test, to_folder), SHOULD_PASS)

################################################################################
# All possible benchmark tests
################################################################################
fancy_title("Tests that should pass (all possible benchmarks")
for test in all_tests:
    to_folder = os.path.join(env, "tmp/benchmark_{0:02d}".format(test))
    system("python question.py --benchmark {0} --to {1}".format(test, to_folder), SHOULD_PASS)
